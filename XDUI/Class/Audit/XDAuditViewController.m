//
//  XDAuditViewController.m
//  XDUI
//
//  Created by xie yajie on 13-10-22.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDAuditViewController.h"

#import "XDActivityModel.h"
#import "XDActivityView.h"
#import "XDDealPicker.h"
#import "XDCreateAuditViewController.h"
#import "XDEditAuditViewController.h"

@interface XDAuditViewController ()<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, XDActivityViewDelegate, XDDealPickerDelegate>
{
    UIButton *_auditButton;
    NSInteger _auditState;
    
    NSMutableArray *_dataSource;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) XDDealPicker *dealPicker;

@end

@implementation XDAuditViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        _auditState = 0;
        
        _dataSource = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"审批";
    
    [self.view addSubview:self.headerView];
    
    self.tableView.frame = CGRectMake(0, self.headerView.frame.origin.y + self.headerView.frame.size.height, self.view.frame.size.width, self.sizeHeight - (self.headerView.frame.origin.y + self.headerView.frame.size.height));
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
    self.tableView.rowHeight = 60.0;
    [self.view addSubview:self.tableView];
    
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(creatAudit:)];
    self.navigationItem.rightBarButtonItem = newItem;
    
    [self configurationData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    
    return _tableView;
}

- (UIView *)headerView
{
    if (_headerView == nil) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.originY, self.view.frame.size.width, 40.0)];
        _headerView.backgroundColor = [UIColor colorWithRed:237 / 255.0 green:237 / 255.0 blue:237 / 255.0 alpha:1.0];
        
        _auditButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, _headerView.frame.size.width / 3, _headerView.frame.size.height - 1)];
        [_auditButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_auditButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [_auditButton setImage:[UIImage imageNamed:@"arrowDown.png"] forState:UIControlStateNormal];
        [_auditButton setImage:[UIImage imageNamed:@"arrowUp.png"] forState:UIControlStateSelected];
        [_auditButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
        [_auditButton setImageEdgeInsets:UIEdgeInsetsMake(0, 85, 0, 30)];
        [_auditButton setTitle:@"待审批" forState:UIControlStateNormal];
        [_auditButton addTarget:self action:@selector(headerAuditAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:_auditButton];
        
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(_auditButton.frame.origin.x + _auditButton.frame.size.width - 1, 0, 1, _headerView.frame.size.height - 1)];
        line1.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
        [_headerView addSubview:line1];
        
        UIButton *dateButton = [[UIButton alloc] initWithFrame:CGRectMake(_auditButton.frame.origin.x + _auditButton.frame.size.width, 0, _headerView.frame.size.width / 3, _headerView.frame.size.height - 1)];
        [dateButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [dateButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [dateButton setImage:[UIImage imageNamed:@"arrowDown.png"] forState:UIControlStateNormal];
        [dateButton setImage:[UIImage imageNamed:@"arrowUp.png"] forState:UIControlStateSelected];
        [dateButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
        [dateButton setImageEdgeInsets:UIEdgeInsetsMake(0, 95, 0, 20)];
        [dateButton setTitle:@"所有日期" forState:UIControlStateNormal];
        [dateButton addTarget:self action:@selector(headerDateAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:dateButton];
        
        UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(dateButton.frame.origin.x + dateButton.frame.size.width - 1, 0, 1, _headerView.frame.size.height - 1)];
        line2.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
        [_headerView addSubview:line2];
        
        UIButton *employeeButton = [[UIButton alloc] initWithFrame:CGRectMake(dateButton.frame.origin.x + dateButton.frame.size.width, 0, _headerView.frame.size.width / 3, _headerView.frame.size.height - 1)];
        [employeeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [employeeButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [employeeButton setImage:[UIImage imageNamed:@"arrowDown.png"] forState:UIControlStateNormal];
        [employeeButton setImage:[UIImage imageNamed:@"arrowUp.png"] forState:UIControlStateSelected];
        [employeeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
        [employeeButton setImageEdgeInsets:UIEdgeInsetsMake(0, 95, 0, 20)];
        [employeeButton setTitle:@"所有员工" forState:UIControlStateNormal];
        [employeeButton addTarget:self action:@selector(headerEmployeeAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:employeeButton];
        
        UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(0, _headerView.frame.size.height - 1, _headerView.frame.size.width, 1)];
        line3.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
        [_headerView addSubview:line3];
    }
    
    return _headerView;
}

- (XDDealPicker *)dealPicker
{
    if (_dealPicker == nil) {
        _dealPicker = [[XDDealPicker alloc] initWithFrame:CGRectZero];
        _dealPicker.delegate = self;
    }
    
    return _dealPicker;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    XDActivityModel *model = [self activityModelAtIndexPath:indexPath];
    XDActivityView *activityView = [[XDActivityView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 0.0) activity:model];
    activityView.delegate = self;
    activityView.otherInfo = @{@"indexPath": indexPath};
    [activityView setupActivityView];
    [cell.contentView addSubview:activityView];
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XDActivityModel *model = [self activityModelAtIndexPath:indexPath];
    
    if (model) {
        XDActivityView *activityView = [[XDActivityView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 0.0) activity:model];
//        activityView.foldState = !activity.unfoldState;
        return [activityView activityViewHeight];
    }
    
    return 44.0;//“更多”
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    // ...
    // Pass the selected object to the new view controller.
}

#pragma mark - XDActivityViewDelegate

- (void)contactActivityViewCommentButtonClicked:(XDActivityView *)activityView
{
    
}

- (void)contactActivityViewAuditButtonClicked:(XDActivityView *)activityView
{
    [self.dealPicker showToView:self.view];
}

#pragma mark - XDDealPickerDelegate

- (void)dealPicker:(XDDealPicker *)dealPicker didSelectedIndex:(NSInteger)selectedIndex
{
    XDEditAuditViewController *editVC = [[XDEditAuditViewController alloc] initWithStyle:UITableViewStyleGrouped  auditState:selectedIndex];
    [self.navigationController pushViewController:editVC animated:YES];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        if (_auditState != buttonIndex) {
            _auditState = buttonIndex;
            
            if (buttonIndex == 0) {//未审核
                [_auditButton setTitle:@"待审批" forState:UIControlStateNormal];
                
                //更换数据
                //????
            }
            else if (buttonIndex == 1)//已审核
            {
                [_auditButton setTitle:@"已审批" forState:UIControlStateNormal];
                
                //更换数据
                //????
            }
            
//            _auditButton.selected = !_auditButton.selected;
        }
    }
}

#pragma mark - button/item action

- (void)creatAudit:(id)sender
{
    XDCreateAuditViewController *createVC = [[XDCreateAuditViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:createVC animated:YES];
}

- (void)headerAuditAction:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"审核状态" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"待审核" otherButtonTitles:@"已审核", nil];
    [actionSheet showInView:self.view];
}

- (void)headerDateAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
}

- (void)headerEmployeeAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
}

#pragma mark - data

- (XDActivityModel *)activityModelAtIndexPath:(NSIndexPath *)indexPath
{
    if (!indexPath) return nil;
    
    if (indexPath.row < [_dataSource count]) {
        return [_dataSource objectAtIndex:indexPath.row];
    }
    
    return nil;
}

- (void)configurationData
{
    //服务器获取数据
    //????
    
    //测试数据
    
    //附件图片
    NSMutableDictionary *appendix = [NSMutableDictionary dictionary];
    
    NSDictionary *image1 = [NSDictionary dictionaryWithObjectsAndKeys:@"1", KMODEL_ID, @"1", KMODEL_ACCOUNTID, @"1.png", KMODEL_NAME, @"1.png", KAPPENDIX_SOURCEPATH, nil];
    NSDictionary *image2 = [NSDictionary dictionaryWithObjectsAndKeys:@"2", KMODEL_ID, @"2", KMODEL_ACCOUNTID, @"2.png", KMODEL_NAME, @"2.png", KAPPENDIX_SOURCEPATH, nil];
    NSArray *images = [NSArray arrayWithObjects:image1, image2, nil];
    
    [appendix setObject:images forKey:KAPPENDIX_IMAGE];
    
    //审核动态
    NSDictionary *audit1 = [NSDictionary dictionaryWithObjectsAndKeys:@"1", KMODEL_ID, @"1", KMODEL_ACCOUNTID, @"程晓玉", KMODEL_NAME, [NSDate date], KMODEL_CREATEDATE, @"同意。", KMODEL_CONTENT, @"0", KMODEL_STATE, nil];
    NSDictionary *audit2 = [NSDictionary dictionaryWithObjectsAndKeys:@"2", KMODEL_ID, @"2", KMODEL_ACCOUNTID, @"陈军", KMODEL_NAME, [NSDate date], KMODEL_CREATEDATE, @"不合理！", KMODEL_CONTENT, @"-1", KMODEL_STATE, nil];
//    NSDictionary *audit3 = [NSDictionary dictionaryWithObjectsAndKeys:@"3", KMODEL_ID, @"3", KMODEL_ACCOUNTID, @"陆家涛", KMODEL_NAME, [NSDate date], KMODEL_CREATEDATE, @"厉害啊！", KMODEL_CONTENT, nil];
    NSArray *audits = [NSArray arrayWithObjects:audit1, audit2, nil];
    
    //评论
    NSDictionary *comment1 = [NSDictionary dictionaryWithObjectsAndKeys:@"1", KMODEL_ID, @"1", KMODEL_ACCOUNTID, @"Avery", KMODEL_NAME, [NSDate date], KMODEL_CREATEDATE, @"不错不错，继续努力。抓紧时间争取再签单！", KMODEL_CONTENT, nil];
    NSDictionary *comment2 = [NSDictionary dictionaryWithObjectsAndKeys:@"2", KMODEL_ID, @"2", KMODEL_ACCOUNTID, @"陈军", KMODEL_NAME, [NSDate date], KMODEL_CREATEDATE, @"加油加油！", KMODEL_CONTENT, nil];
    NSDictionary *comment3 = [NSDictionary dictionaryWithObjectsAndKeys:@"3", KMODEL_ID, @"3", KMODEL_ACCOUNTID, @"陆家涛", KMODEL_NAME, [NSDate date], KMODEL_CREATEDATE, @"厉害啊！", KMODEL_CONTENT, nil];
    NSArray *comments = [NSArray arrayWithObjects:comment1, comment2, comment3, nil];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"1", KMODEL_ID, @"1", KMODEL_ACCOUNTID, @"xieyajie", KMODEL_NAME, @"", KMODEL_HEADPATH, [NSDate date], KMODEL_CREATEDATE, @"已提交，待程小雨审批。", KMODEL_TITLE, @"北京-石家庄出差往返机票。参加石家庄移动竞标项目。", KMODEL_CONTENT, @"北京市海淀区海淀西大街48号图书城步行街内鑫鼎宾馆", KMODEL_ADDRESS, @"0", KMODEL_STATE, audits, KMODEL_AUDITLIST, comments, KMODEL_COMMENTLIST, appendix, KAPPENDIX, nil];
    
    XDActivityModel *activityModel = [[XDActivityModel alloc] initWithDictionary:dic];
    [_dataSource addObject:activityModel];
    
    [self.tableView reloadData];
}

@end
