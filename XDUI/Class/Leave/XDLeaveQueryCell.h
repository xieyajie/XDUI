//
//  XDLeaveQueryCell.h
//  XDUI
//
//  Created by xieyajie on 13-11-7.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XDLeaveQueryCell : UITableViewCell
{
    UIImageView *_headerView;
    UILabel *_nameLabel;
    UILabel *_dateLabel;
    UILabel *_contentLabel;
}

@property (strong, nonatomic) NSString * headerImageName;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * dateString;
@property (strong, nonatomic) NSString * content;

@end
