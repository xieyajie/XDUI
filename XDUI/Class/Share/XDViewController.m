//
//  XDViewController.m
//  XDUI
//
//  Created by xieyajie on 13-10-14.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDViewController.h"

@interface XDViewController ()

@end

@implementation XDViewController

@synthesize originY = _originY;
@synthesize sizeHeight = _sizeHeight;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _originY = 0;
    _sizeHeight = [[UIScreen mainScreen] bounds].size.height - 20;
    self.view.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    
    if (!self.navigationController.navigationBarHidden) {
        _sizeHeight -= self.navigationController.navigationBar.frame.size.height;
    }
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        self.extendedLayoutIncludesOpaqueBars = NO;
//        _originY = self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (CGFloat)originY
{
    return _originY;
}

- (CGFloat)sizeHeight
{
    return _sizeHeight;
}

@end
