//
//  XDLeaveViewController.m
//  XDUI
//
//  Created by xieyajie on 13-11-7.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDLeaveViewController.h"

#import "XDPublicView.h"
#import "XDLeaveQueryViewController.h"
#import "XDLeaveOperateViewController.h"

@interface XDLeaveViewController ()

@property (strong, nonatomic) XDPublicView *applyView;
@property (strong, nonatomic) XDPublicView *queryView;

@end

@implementation XDLeaveViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"请假管理";
    
    [self.view addSubview:self.applyView];
    [self.view addSubview:self.queryView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (XDPublicView *)applyView
{
    if (_applyView == nil) {
        _applyView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 20, self.view.frame.size.width - 20, 40)];
        _applyView.titleLabel.text = @"请假申请";
        [_applyView addTarget:self action:@selector(applyAction:)];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 40)];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.backgroundColor = [UIColor yellowColor];
        imgView.image = [UIImage imageNamed:@""];
        _applyView.rightAccessoryView = imgView;
    }
    
    return _applyView;
}

- (XDPublicView *)queryView
{
    if (_queryView == nil) {
        _queryView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, self.applyView.frame.origin.y + self.applyView.frame.size.height + 20, self.view.frame.size.width - 20, 40)];
        _queryView.titleLabel.text = @"请假查询";
        [_queryView addTarget:self action:@selector(queryAction:)];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 40)];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.backgroundColor = [UIColor yellowColor];
        imgView.image = [UIImage imageNamed:@""];
        _queryView.rightAccessoryView = imgView;
    }
    
    return _queryView;
}

#pragma mark - action

- (void)applyAction:(id)sender
{
    XDLeaveOperateViewController *operateVC = [[XDLeaveOperateViewController alloc] initWithStyle:UITableViewStylePlain];
    operateVC.title = @"请假申请";
    [self.navigationController pushViewController:operateVC animated:YES];
}

- (void)queryAction:(id)sender
{
    XDLeaveQueryViewController *queryVC = [[XDLeaveQueryViewController alloc] init];
    [self.navigationController pushViewController:queryVC animated:YES];
}

@end
