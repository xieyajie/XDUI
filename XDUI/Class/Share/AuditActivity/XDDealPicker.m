//
//  XDDealPicker.m
//  XDUI
//
//  Created by xieyajie on 13-11-1.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDDealPicker.h"

@implementation XDDealPicker

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor colorWithWhite:0.4 alpha:0.5];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapShadow:)];
        [self addGestureRecognizer:tap];
        
        _dealView = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 180, 35)];
        _dealView.backgroundColor = [UIColor blackColor];
        [self addSubview:_dealView];
        
        _disagreeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 60, 25)];
        _disagreeButton.backgroundColor = [UIColor redColor];
        _disagreeButton.tag = 0;
        [_disagreeButton setTitle:@"不同意" forState:UIControlStateNormal];
        _disagreeButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_disagreeButton addTarget:self action:@selector(chooseDeal:) forControlEvents:UIControlEventTouchUpInside];
        [_dealView addSubview:_disagreeButton];
        
        _reviewButton = [[UIButton alloc] initWithFrame:CGRectMake(_disagreeButton.frame.origin.x + _disagreeButton.frame.size.width + 5, 5, 50, 25)];
        _reviewButton.backgroundColor = [UIColor orangeColor];
        _reviewButton.tag = 1;
        [_reviewButton setTitle:@"复议" forState:UIControlStateNormal];
        _reviewButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_reviewButton addTarget:self action:@selector(chooseDeal:) forControlEvents:UIControlEventTouchUpInside];
        [_dealView addSubview:_reviewButton];
        
        _agreeButton = [[UIButton alloc] initWithFrame:CGRectMake(_reviewButton.frame.origin.x + _reviewButton.frame.size.width + 5, 5, 50, 25)];
        _agreeButton.backgroundColor = [UIColor greenColor];
        _agreeButton.tag = 2;
        [_agreeButton setTitle:@"同意" forState:UIControlStateNormal];
        _agreeButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_agreeButton addTarget:self action:@selector(chooseDeal:) forControlEvents:UIControlEventTouchUpInside];
        [_dealView addSubview:_agreeButton];
    }
    return self;
}

- (NSInteger)agreeIndex
{
    return 2;
}

- (NSInteger)disagreeIndex
{
    return 0;
}

- (NSInteger)reviewIndex
{
    return 1;
}

- (void)tapShadow:(UITapGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:.5f animations:^{
            [self removeFromSuperview];
        }];
    }
}

- (void)chooseDeal:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (_delegate && [_delegate respondsToSelector:@selector(dealPicker:didSelectedIndex:)]) {
        [_delegate dealPicker:self didSelectedIndex:button.tag];
        [self removeFromSuperview];
    }
}

- (void)showToView:(UIView *)view
{
    _dealView.alpha = 0;
    
    CGPoint point = CGPointMake((view.frame.size.width - _dealView.frame.size.width) / 2, (view.frame.size.height - _dealView.frame.size.height) / 2);
    self.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    _dealView.frame = CGRectMake(point.x, point.y, _dealView.frame.size.width, _dealView.frame.size.height);
    [view addSubview:self];
    
    [UIView animateWithDuration:.5f animations:^{
        _dealView.alpha = 1.0;
    }];
}

@end
