//
//  XDActivityModel.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDModel.h"

@interface XDActivityModel : XDModel

@property (nonatomic, copy) NSString *stateDescription;//动态的状态描述
@property (nonatomic, copy) NSString *finance;//动态的财务金额

@property (nonatomic, copy) NSMutableArray *auditList;//动态的审批列表
@property (nonatomic, copy) NSMutableArray *commentList;//动态的评论列表

@end
