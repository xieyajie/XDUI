//
//  XDAuditModel.m
//  XDUI
//
//  Created by xie yajie on 13-10-26.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDAuditModel.h"

@implementation XDAuditModel

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.ID = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ID] integerValue]];
        self.accountId = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ACCOUNTID] integerValue]];
        self.name = [dictionary objectForKey:KMODEL_NAME];
        self.headImagePath = [dictionary objectForKey:KMODEL_HEADPATH];
//        self.gender = [dictionary objectForKey:KMODEL_GENDER];
        self.createDate = [dictionary objectForKey:KMODEL_CREATEDATE];
        
        self.content = [dictionary objectForKey:KMODEL_CONTENT];
        
        //审批状态
        self.state = [[dictionary objectForKey:KMODEL_STATE] integerValue];
        self.stateDescription = [self descriptionForState:self.state];
    }
    
    return self;
}

- (NSString *)descriptionForState:(NSInteger)code
{
    switch (code) {
        case -1:
            return @"不同意";
            break;
        case 0:
            return @"同意";
            break;
            
        default:
            return @"";
            break;
    }
}

@end
