//
//  XDClockDetailCell.m
//  XDUI
//
//  Created by xieyajie on 13-10-14.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDClockDetailCell.h"

#import "XDPublicView.h"

@implementation XDClockDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _publicDetailView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width - 20, self.frame.size.height - 10) style:XDPublicViewStyleDetail];
        [self.contentView addSubview:_publicDetailView];
        
        _publicImageView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width - 20, self.frame.size.height - 10) style:XDPublicViewStyleImage];
        
        _errorButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 300, 40)];
        [_errorButton setBackgroundImage:[UIImage imageNamed:@"buttonBg_normal.png"] forState:UIControlStateNormal];
        [_errorButton setBackgroundImage:[UIImage imageNamed:@"buttonBg_highlighted.png"] forState:UIControlStateHighlighted];
        [_errorButton addTarget:self action:@selector(errorButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        _errorButton.backgroundColor = [UIColor redColor];;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - setting

- (void)setTitle:(NSString *)title
{
    [_errorButton removeFromSuperview];
    _publicDetailView.titleLabel.text = title;
    _publicImageView.titleLabel.text = title;
}

- (void)setDetailText:(NSString *)detailText
{
    [_errorButton removeFromSuperview];
    [_publicImageView removeFromSuperview];
    [self addSubview:_publicDetailView];
    
    _publicDetailView.detailLabel.text = detailText;
    CGRect rect = _publicDetailView.frame;
    _publicDetailView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, [_publicDetailView heightWithDetailText:detailText]);
}

- (void)setTextColor:(UIColor *)textColor
{
    _publicDetailView.detailLabel.textColor = textColor;
}

#pragma mark - public

- (void)errorButtonAction:(id)sender
{
    [_rootTableView.delegate tableView: _rootTableView
                   didSelectRowAtIndexPath: [_rootTableView indexPathForCell: self]];
}

- (void)showButtonToTableView:(UITableView *)tableView title:(NSString *)title
{
    [_publicDetailView removeFromSuperview];
    [_publicImageView removeFromSuperview];
    
    _rootTableView = tableView;
    [_errorButton setTitle:title forState:UIControlStateNormal];
    [self.contentView addSubview:_errorButton];
    [self.contentView bringSubviewToFront:_errorButton];
}

- (void)setupWithImageSource:(NSArray *)array canEdit:(BOOL)edit
{
    [_errorButton removeFromSuperview];
    [_publicDetailView removeFromSuperview];
    [self addSubview:_publicImageView];
    
    CGRect rect = _publicImageView.frame;
    _publicImageView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, [_publicImageView heightForImageState]);
    
    [_publicImageView setupWithImageSource:array canEdit:edit];
}

@end
