//
//  XDDealPicker.h
//  XDUI
//
//  Created by xieyajie on 13-11-1.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol XDDealPickerDelegate;

@interface XDDealPicker : UIView
{
    UIView *_dealView;
    
    UIButton *_agreeButton;
    UIButton *_disagreeButton;
    UIButton *_reviewButton;
}

@property (nonatomic, unsafe_unretained) id<XDDealPickerDelegate> delegate;

@property (nonatomic) NSInteger agreeIndex;
@property (nonatomic) NSInteger disagreeIndex;
@property (nonatomic) NSInteger reviewIndex;

- (void)showToView:(UIView *)view;

@end

@protocol XDDealPickerDelegate <NSObject>

@required
- (void)dealPicker:(XDDealPicker *)dealPicker didSelectedIndex:(NSInteger)selectedIndex;

@end
