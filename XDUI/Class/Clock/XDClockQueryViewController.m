//
//  XDClockQueryViewController.m
//  XDUI
//
//  Created by xie yajie on 13-10-13.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDClockQueryViewController.h"

#import "XDClockQueryCell.h"
#import "XDClockDetailViewController.h"

#define KCLOCKQUERY_HEADERIMAGE @"headerImage"
#define KCLOCKQUERY_NAME @"name"
#define KCLOCKQUERY_DATE @"date"
#define KCLOCKQUERY_DUTY @"duty"
#define KCLOCKQUERY_DUTY_ON @"onDuty"
#define KCLOCKQUERY_DUTY_OFF @"offDuty"
#define KCLOCKQUERY_DUTY_TIME @"duty_time"
#define KCLOCKQUERY_DUTY_ADDRESS @"duty_address"
#define KCLOCKQUERY_DUTY_REMARK @"duty_remark"
#define KCLOCKQUERY_DUTY_AUDIT @"duty_audit"

@interface XDClockQueryViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *_dataSource;
    NSDateFormatter *_dateFormatter;
    NSDateFormatter *_timeFormatter;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *headerView;

@end

@implementation XDClockQueryViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        _dataSource = [NSMutableArray array];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
        _timeFormatter = [[NSDateFormatter alloc] init];
        _timeFormatter.dateFormat = @"hh:mm";
        
        [self configurationData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    self.title = @"考勤查询";
 
    [self.view addSubview:self.headerView];
    
    self.tableView.frame = CGRectMake(0, self.headerView.frame.origin.y + self.headerView.frame.size.height, self.view.frame.size.width, self.sizeHeight - (self.headerView.frame.origin.y + self.headerView.frame.size.height));
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
    self.tableView.rowHeight = 60.0;
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    
    return _tableView;
}

- (UIView *)headerView
{
    if (_headerView == nil) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.originY, self.view.frame.size.width, 40.0)];
        _headerView.backgroundColor = [UIColor colorWithRed:237 / 255.0 green:237 / 255.0 blue:237 / 255.0 alpha:1.0];
        
        UIButton *dateButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, _headerView.frame.size.width / 2 - 1, _headerView.frame.size.height - 1)];
        [dateButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [dateButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [dateButton setImage:[UIImage imageNamed:@"arrowDown.png"] forState:UIControlStateNormal];
        [dateButton setImage:[UIImage imageNamed:@"arrowUp.png"] forState:UIControlStateSelected];
        [dateButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
        [dateButton setImageEdgeInsets:UIEdgeInsetsMake(0, 105, 0, 30)];
        [dateButton setTitle:@"所有日期" forState:UIControlStateNormal];
        [dateButton addTarget:self action:@selector(headerDateAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:dateButton];
        
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(_headerView.frame.size.width / 2 - 1, 0, 1, _headerView.frame.size.height)];
        line1.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
        [_headerView addSubview:line1];
        
        UIButton *employeeButton = [[UIButton alloc] initWithFrame:CGRectMake(_headerView.frame.size.width / 2, 0, _headerView.frame.size.width / 2, _headerView.frame.size.height - 1)];
        [employeeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [employeeButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [employeeButton setImage:[UIImage imageNamed:@"arrowDown.png"] forState:UIControlStateNormal];
        [employeeButton setImage:[UIImage imageNamed:@"arrowUp.png"] forState:UIControlStateSelected];
        [employeeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
        [employeeButton setImageEdgeInsets:UIEdgeInsetsMake(0, 105, 0, 30)];
        [employeeButton setTitle:@"所有员工" forState:UIControlStateNormal];
        [employeeButton addTarget:self action:@selector(headerEmployeeAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:employeeButton];
        
        UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, _headerView.frame.size.height - 1, _headerView.frame.size.width, 1)];
        line2.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
        [_headerView addSubview:line2];
    }
    
    return _headerView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    XDClockQueryCell *cell = (XDClockQueryCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[XDClockQueryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    if (dic) {
        cell.headerImageName = [dic objectForKey:KCLOCKQUERY_HEADERIMAGE];
        cell.name = [dic objectForKey:KCLOCKQUERY_NAME];
        cell.dateString = [_dateFormatter stringFromDate:[dic objectForKey:KCLOCKQUERY_DATE]];
        
        NSDictionary *dutyDic = [dic objectForKey:KCLOCKQUERY_DUTY];
        if (dutyDic) {
            NSDictionary *onDic = [dutyDic objectForKey:KCLOCKQUERY_DUTY_ON];
            if (onDic) {
                id onObj = [onDic objectForKey:KCLOCKQUERY_DUTY_TIME];
                NSString *onTime = onObj == nil ? @"未打卡" : [_timeFormatter stringFromDate:onObj];
                cell.onDutyView.title = [NSString stringWithFormat:@"上班：%@", onTime];
                
                cell.onDutyView.addressState = [[onDic objectForKey:KCLOCKQUERY_DUTY_ADDRESS] integerValue];
                cell.onDutyView.remarkState = [[onDic objectForKey:KCLOCKQUERY_DUTY_REMARK] integerValue];
                cell.onDutyView.auditState = [[onDic objectForKey:KCLOCKQUERY_DUTY_AUDIT] integerValue];
            }
            else{
                cell.onDutyView.title = @"上班：未打卡";
                cell.onDutyView.addressState = XDClockDutyInfoStateNone;
                cell.onDutyView.remarkState = XDClockDutyInfoStateNone;
                cell.onDutyView.auditState = XDClockDutyInfoStateNone;
                cell.onDutyView.error = YES;
            }
            
            NSDictionary *offDic = [dutyDic objectForKey:KCLOCKQUERY_DUTY_OFF];
            if (offDic) {
                id offObj = [offDic objectForKey:KCLOCKQUERY_DUTY_TIME];
                NSString *offTime = offObj == nil ? @"未打卡" : [_timeFormatter stringFromDate:offObj];
                cell.offDutyView.title = [NSString stringWithFormat:@"下班：%@", offTime];
                
                cell.offDutyView.addressState = [[offDic objectForKey:KCLOCKQUERY_DUTY_ADDRESS] integerValue];
                cell.offDutyView.remarkState = [[offDic objectForKey:KCLOCKQUERY_DUTY_REMARK] integerValue];
                cell.offDutyView.auditState = [[offDic objectForKey:KCLOCKQUERY_DUTY_AUDIT] integerValue];
            }
            else{
                cell.offDutyView.title = @"下班：未打卡";
                cell.offDutyView.addressState = XDClockDutyInfoStateNone;
                cell.offDutyView.remarkState = XDClockDutyInfoStateNone;
                cell.offDutyView.auditState = XDClockDutyInfoStateNone;
                cell.offDutyView.error = YES;
            }
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
     XDClockDetailViewController *detailViewController = [[XDClockDetailViewController alloc] init];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     
}

#pragma mark - data

- (void)configurationData
{
    //服务器获取数据
    //????
    
    //测试数据
    NSDictionary *dutyDic1 = [NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:[NSDate date], KCLOCKQUERY_DUTY_TIME, @"0", KCLOCKQUERY_DUTY_ADDRESS, @"0", KCLOCKQUERY_DUTY_REMARK, @"1", KCLOCKQUERY_DUTY_AUDIT, nil], KCLOCKQUERY_DUTY_ON, [NSDictionary dictionaryWithObjectsAndKeys:[NSDate date], KCLOCKQUERY_DUTY_TIME, @"-1", KCLOCKQUERY_DUTY_ADDRESS, @"1", KCLOCKQUERY_DUTY_REMARK, @"1", KCLOCKQUERY_DUTY_AUDIT, nil], KCLOCKQUERY_DUTY_OFF, nil];
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"clock_headerImageDefult.png", KCLOCKQUERY_HEADERIMAGE, @"Anne", KCLOCKQUERY_NAME, [NSDate date], KCLOCKQUERY_DATE, dutyDic1, KCLOCKQUERY_DUTY, nil];
    
    NSDictionary *dutyDic2 = [NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:[NSDate date], KCLOCKQUERY_DUTY_TIME, @"1", KCLOCKQUERY_DUTY_ADDRESS, @"0", KCLOCKQUERY_DUTY_REMARK, @"-1", KCLOCKQUERY_DUTY_AUDIT, nil], KCLOCKQUERY_DUTY_ON, [NSDictionary dictionaryWithObjectsAndKeys:[NSDate date], KCLOCKQUERY_DUTY_TIME, @"-1", KCLOCKQUERY_DUTY_ADDRESS, @"1", KCLOCKQUERY_DUTY_REMARK, @"1", KCLOCKQUERY_DUTY_AUDIT, nil], KCLOCKQUERY_DUTY_OFF, nil];
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"clock_headerImageDefult.png", KCLOCKQUERY_HEADERIMAGE, @"白鹭", KCLOCKQUERY_NAME, [NSDate date], KCLOCKQUERY_DATE, dutyDic2, KCLOCKQUERY_DUTY, nil];
    
    for (int i = 0; i < 5; i++) {
        [_dataSource addObject:dic1];
        [_dataSource addObject:dic2];
    }
}

#pragma mark - button/item action

- (void)headerDateAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
}

- (void)headerEmployeeAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
}

@end
