//
//  XDClockQueryCell.h
//  XDUI
//
//  Created by xie yajie on 13-10-13.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    XDClockDutyInfoStateError = -1,
    XDClockDutyInfoStateNone  = 0,
    XDClockDutyInfoStateNormal,
}XDClockDutyInfoState;

@interface XDClockDutyView : UIView

@property (strong, nonatomic) NSString *title;
@property (nonatomic) BOOL error;

@property (nonatomic) XDClockDutyInfoState addressState;
@property (nonatomic) XDClockDutyInfoState remarkState;
@property (nonatomic) XDClockDutyInfoState auditState;

@end

@interface XDClockQueryCell : UITableViewCell
{
    UIImageView *_headerView;
    UILabel *_nameLabel;
    UILabel *_dateLabel;
    XDClockDutyView *_onDutyView;
    XDClockDutyView *_offDutyView;
}

@property (strong, nonatomic) NSString * headerImageName;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * dateString;

@property (strong, nonatomic) XDClockDutyView *onDutyView;
@property (strong, nonatomic) XDClockDutyView *offDutyView;

@end
