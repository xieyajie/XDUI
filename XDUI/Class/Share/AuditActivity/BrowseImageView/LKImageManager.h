//
//  LKImageManager.h
//  Travel
//
//  Created by tixa tixa on 13-4-23.
//  Copyright (c) 2013年 tixa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LKImageManager : NSObject<UIScrollViewDelegate>

- (void)showWithScrollList:(NSArray *)scrollList;

- (void)scrollToIndex:(int)index;

- (void)removeScrollView;

@property (nonatomic) CGPoint touchPoint;    //触摸点
@property (nonatomic) CGSize  touchSize;     //触摸视图大小
@property (nonatomic) NSInteger touchIndex;
@property (nonatomic) NSInteger column; //默认 4； 布局的列数
@property (nonatomic) float     edge;   //默认 15；布局的边距
@property (nonatomic) float     corner; //默认 6； 布局的圆角

@end
