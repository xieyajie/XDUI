//
//  XDAppendixModel.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XDAppendixModel : NSObject

@property (nonatomic, strong) NSString *ID;                //文件id
@property (nonatomic, strong) NSString *name;              //文件名
@property (nonatomic) CGFloat duration;                    //时长(秒)。音频、视频总时长。
@property (nonatomic) CGFloat size;                        //大小(字节)。音频、视频文件大小。
@property (nonatomic, strong) NSString *sourcePath;        //源文件路径(图片大图，GIF、音频、视频源文件)，此值不可为空

@property (nonatomic) NSInteger type;                      //类型
@property (nonatomic) NSInteger subtype;                   //子类型
@property (nonatomic, strong) NSString *mediumImagePath;   //中图路径(图片、GIF截图、视频截图的中图）
@property (nonatomic, strong) NSString *smallImagePath;    //小图路径(图片、GIF截图、视频截图的缩略图）

@property (nonatomic, strong) NSObject *relatedObject;     //关联对象

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (NSString *)durationDescription;  //时长描述
- (NSString *)sizeDescription;      //大小描述

@end
