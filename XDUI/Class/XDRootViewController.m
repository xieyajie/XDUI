//
//  XDRootViewController.m
//  XDUI
//
//  Created by xie yajie on 13-10-12.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XDRootViewController.h"

#import "XDClockViewController.h"
#import "XDAuditViewController.h"
#import "XDNoticeViewController.h"
#import "XDLeaveViewController.h"

@interface XDRootViewController ()
{
    UIButton *_clockButton;
    UIButton *_auditButton;
    UIButton *_noticeButton;
    UIButton *_leaveButton;
    UIButton *_addressButton;
}

@end

@implementation XDRootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _clockButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, self.view.frame.size.width - 40, 50)];
    [_clockButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_clockButton setTitle:@"考勤打卡" forState:UIControlStateNormal];
    [_clockButton addTarget:self action:@selector(clockAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_clockButton];
    _clockButton.layer.borderColor = [[UIColor grayColor] CGColor];
    _clockButton.layer.borderWidth = 1.0;
    
    _auditButton = [[UIButton alloc] initWithFrame:CGRectMake(20, _clockButton.frame.origin.y + _clockButton.frame.size.height + 20, self.view.frame.size.width - 40, 50)];
    [_auditButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_auditButton setTitle:@"审核" forState:UIControlStateNormal];
    [_auditButton addTarget:self action:@selector(auditAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_auditButton];
    _auditButton.layer.borderColor = [[UIColor grayColor] CGColor];
    _auditButton.layer.borderWidth = 1.0;
    
    _noticeButton = [[UIButton alloc] initWithFrame:CGRectMake(20, _auditButton.frame.origin.y + _auditButton.frame.size.height + 20, self.view.frame.size.width - 40, 50)];
    [_noticeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_noticeButton setTitle:@"企业公告" forState:UIControlStateNormal];
    [_noticeButton addTarget:self action:@selector(noticeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_noticeButton];
    _noticeButton.layer.borderColor = [[UIColor grayColor] CGColor];
    _noticeButton.layer.borderWidth = 1.0;
    
    _leaveButton = [[UIButton alloc] initWithFrame:CGRectMake(20, _noticeButton.frame.origin.y + _noticeButton.frame.size.height + 20, self.view.frame.size.width - 40, 50)];
    [_leaveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_leaveButton setTitle:@"请假管理" forState:UIControlStateNormal];
    [_leaveButton addTarget:self action:@selector(leaveAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_leaveButton];
    _leaveButton.layer.borderColor = [[UIColor grayColor] CGColor];
    _leaveButton.layer.borderWidth = 1.0;
    
    _addressButton = [[UIButton alloc] initWithFrame:CGRectMake(20, _leaveButton.frame.origin.y + _leaveButton.frame.size.height + 20, self.view.frame.size.width - 40, 50)];
    [_addressButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_addressButton setTitle:@"位置查询" forState:UIControlStateNormal];
    [_addressButton addTarget:self action:@selector(addressAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_addressButton];
    _addressButton.layer.borderColor = [[UIColor grayColor] CGColor];
    _addressButton.layer.borderWidth = 1.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button action

- (void)clockAction:(id)sender
{
    XDClockViewController *clockVC = [[XDClockViewController alloc] init];
    [self.navigationController pushViewController:clockVC animated:YES];
}

- (void)auditAction:(id)sender
{
    XDAuditViewController *auditVC = [[XDAuditViewController alloc] init];
    [self.navigationController pushViewController:auditVC animated:YES];
}

- (void)noticeAction:(id)sender
{
    XDNoticeViewController *noticeVC = [[XDNoticeViewController alloc] init];
    [self.navigationController pushViewController:noticeVC animated:YES];
}

- (void)leaveAction:(id)sender
{
    XDLeaveViewController *leaveVC = [[XDLeaveViewController alloc] init];
    [self.navigationController pushViewController:leaveVC animated:YES];
}

- (void)addressAction:(id)sender
{
//    [self.navigationController pushViewController:leaveVC animated:YES];
}

@end
