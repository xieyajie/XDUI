//
//  XDNoticeViewController.m
//  XDUI
//
//  Created by xieyajie on 13-11-5.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDNoticeViewController.h"

#import "XDNoticeCell.h"
#import "XDNoticeDetailViewController.h"
#import "XDCreateNoticeViewController.h"

@interface XDNoticeViewController ()
{
    NSMutableArray *_dataSource;
}

@end

@implementation XDNoticeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _dataSource = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    self.title = @"企业公告";
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIBarButtonItem *newItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"notice_new.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(createAction:)];
    self.navigationItem.rightBarButtonItem = newItem;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:247 / 255.0 green:247 / 255.0 blue:247 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self configurationData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NoticeCell";
    XDNoticeCell *cell = (XDNoticeCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[XDNoticeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    if (dic) {
        cell.titleLabel.text = [dic objectForKey:KNOTICE_TITLE];
        cell.content = [dic objectForKey:KNOTICE_CONTENT];
        cell.date = [dic objectForKey:KNOTICE_DATE];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    NSString *content = [dic objectForKey:KNOTICE_CONTENT];
    
    CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(280, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    if (size.height < 20) {
        return KNOTICE_CELL_MIN_HEIGHT + 20;
    }
    
    return KNOTICE_CELL_MAX_HEIGHT + 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    XDNoticeDetailViewController *detailVC = [[XDNoticeDetailViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - button/item action

- (void)createAction:(id)sender
{
    XDCreateNoticeViewController *createVC = [[XDCreateNoticeViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.navigationController pushViewController:createVC animated:YES];
}

#pragma mark - data

- (void)configurationData
{
    //从服务器获取数据
    //????
    
    //测试数据
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"五一放假通知", KNOTICE_TITLE, @"公司全体员工：劳动节将来临，根据公司规定5.1劳动节放假一天，祝大家节日快乐！", KNOTICE_CONTENT, [NSDate date], KNOTICE_DATE, nil];
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"关于强化销售贷款回笼资金工作", KNOTICE_TITLE, @"销售部，财务部：为保障企业正常生产经营需", KNOTICE_CONTENT, [NSDate date], KNOTICE_DATE, nil];
    NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"关于强化销售贷款回笼资金工作", KNOTICE_TITLE, @"销售部，财务部：为保障企业正常生产经营需要，强化提高销售人员对销售贷款工作的责任", KNOTICE_CONTENT, [NSDate date], KNOTICE_DATE, nil];
    
    [_dataSource addObject:dic1];
    [_dataSource addObject:dic2];
    [_dataSource addObject:dic3];
    
//    [self.tableView reloadData];
}

@end
