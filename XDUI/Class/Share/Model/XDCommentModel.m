//
//  XDCommentModel.m
//  XDUI
//
//  Created by xie yajie on 13-10-26.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDCommentModel.h"

@implementation XDCommentModel

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.ID = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ID] integerValue]];
        self.accountId = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ACCOUNTID] integerValue]];
        self.name = [dictionary objectForKey:KMODEL_NAME];
        self.headImagePath = [dictionary objectForKey:KMODEL_HEADPATH];
        //        self.gender = [dictionary objectForKey:KMODEL_GENDER];
        self.createDate = [dictionary objectForKey:KMODEL_CREATEDATE];
        
        self.content = [dictionary objectForKey:KMODEL_CONTENT];
    }
    
    return self;
}

@end
