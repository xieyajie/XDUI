//
//  XDLeaveOperateViewController.m
//  XDUI
//
//  Created by xieyajie on 13-11-7.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDLeaveOperateViewController.h"

#import "XDPublicView.h"
#import "XDRemarkViewController.h"

#define KCELLSECTION_BEGINDATE 0
#define KCELLSECTION_ENDDATE 1
#define KCELLSECTION_DAYS 2
#define KCELLSECTION_TYPE 3
#define KCELLSECTION_DESCRIBE 4

@interface XDLeaveOperateViewController ()
{
    BOOL _isCreate;
    
    NSMutableDictionary *_dataSource;
    NSMutableArray *_viewsArray;
    
    NSDateFormatter *_dateFormatter;
    NSDateFormatter *_timeFormatter;
}

@property (strong, nonatomic) XDPublicView *beginDateView;
@property (strong, nonatomic) XDPublicView *beginTimeView;
@property (strong, nonatomic) XDPublicView *endDateView;
@property (strong, nonatomic) XDPublicView *endTimeView;
@property (strong, nonatomic) XDPublicView *daysView;
@property (strong, nonatomic) XDPublicView *typeView;
@property (strong, nonatomic) XDPublicView *describeView;

@end

@implementation XDLeaveOperateViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _isCreate = YES;
        
        _dataSource = [NSMutableDictionary dictionary];
        _viewsArray = [NSMutableArray array];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
        _timeFormatter = [[NSDateFormatter alloc] init];
        _timeFormatter.dateFormat = @"hh:mm";
    }
    return self;
}

- (id)initDetailWithStyle:(UITableViewStyle)style withInfo:(NSDictionary *)info
{
    self = [self initWithStyle:style];
    if (self) {
        _isCreate = NO;
        
        if (info && [info count] > 0) {
            [_dataSource addEntriesFromDictionary:info];
        }
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remarkEditFinish:) name:@"remarkEditFinish" object:nil];
    
    [self configurationViews];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (_isCreate) {
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"doneButtonImage.png"] style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
        self.navigationItem.rightBarButtonItem = doneItem;
    }
    else{
        [self configurationData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mrk - getting

- (XDPublicView *)beginDateView
{
    if (_beginDateView == nil) {
        _beginDateView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.tableView.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _beginDateView.titleLabel.text = @"开始日期";
        if (_isCreate) {
            [_beginDateView addTarget:self action:@selector(beginDateAction:)];
        }
    }
    
    return _beginDateView;
}

- (XDPublicView *)beginTimeView
{
    if (_beginTimeView == nil) {
        _beginTimeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 1, self.tableView.frame.size.width - 20, 39) style:XDPublicViewStyleDetail];
        _beginTimeView.titleLabel.text = @"开始时间";
        if (_isCreate) {
            [_beginTimeView addTarget:self action:@selector(beginTimeAction:)];
        }
    }
    
    return _beginTimeView;
}

- (XDPublicView *)endDateView
{
    if (_endDateView == nil) {
        _endDateView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.tableView.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _endDateView.titleLabel.text = @"结束日期";
        if (_isCreate) {
            [_endDateView addTarget:self action:@selector(endDateAction:)];
        }
    }
    
    return _endDateView;
}

- (XDPublicView *)endTimeView
{
    if (_endTimeView == nil) {
        _endTimeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 1, self.tableView.frame.size.width - 20, 39) style:XDPublicViewStyleDetail];
        _endTimeView.titleLabel.text = @"结束时间";
        if (_isCreate) {
            [_endTimeView addTarget:self action:@selector(endTimeAction:)];
        }
    }
    
    return _endTimeView;
}

- (XDPublicView *)daysView
{
    if (_daysView == nil) {
        _daysView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.tableView.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _daysView.titleLabel.text = @"请假天数";
    }
    
    return _daysView;
}

- (XDPublicView *)typeView
{
    if (_typeView == nil) {
        _typeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.tableView.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _typeView.titleLabel.text = @"请假类型";
        if (_isCreate) {
            [_typeView addTarget:self action:@selector(typeAction:)];
        }
    }
    
    return _typeView;
}

- (XDPublicView *)describeView
{
    if (_describeView == nil) {
        if (_isCreate) {
            _describeView = [[XDPublicView alloc] initWithFrame:CGRectZero style:XDPublicViewStyleText];
            CGFloat height = [_describeView heightForTextViewWithString:@""];
            _describeView.frame = CGRectMake(10, 0, self.tableView.frame.size.width - 20, height);
            [_describeView setupForRemarkWithViewJump:YES];
            [_describeView addTarget:self action:@selector(describeAction:)];
        }
        else{
            _describeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.tableView.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        }
        
        _describeView.titleLabel.text = @"事由说明";
    }
    
    return _describeView;
}

#pragma mark - notifation

- (void)remarkEditFinish:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSString class]]) {
        self.describeView.textView.text = (NSString *)notification.object;
        
        CGFloat height = [self.describeView heightForTextViewWithString:self.describeView.textView.text];
        self.describeView.frame = CGRectMake(10, 0, self.tableView.frame.size.width - 20, height);
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:KCELLSECTION_DESCRIBE], nil] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == KCELLSECTION_BEGINDATE || section == KCELLSECTION_ENDDATE) {
        return 2;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LeaveOperateCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    [cell.contentView addSubview:[[_viewsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != KCELLSECTION_DESCRIBE) {
        return 40.0;
    }
    else{
        if (_isCreate) {
            CGFloat height = [self.describeView heightForTextViewWithString:self.describeView.textView.text];
            return height + 20;
        }
        else{
            return [self.describeView heightWithDetailText:[_dataSource objectForKey:KMODEL_CONTENT]] + 20;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20.0)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
}

#pragma mark - private

- (void)configurationData
{
    //从服务器获取数据
    //????
    
    //测试数据
    [_dataSource setObject:[NSDate date] forKey:KLEAVE_BEGINDATE];
    [_dataSource setObject:[NSDate date] forKey:KLEAVE_ENDDATE];
    [_dataSource setObject:@"1" forKey:@"leaveDays"];
    [_dataSource setObject:@"病假" forKey:KLEAVE_TYPE];
    [_dataSource setObject:@"感冒发烧请假一天感冒发烧请假一天感冒发烧请假一天感冒发烧请假一天感冒发烧请假一天" forKey:KMODEL_CONTENT];
    
    //数据申请成功后，付给view
    self.beginDateView.detailLabel.text = [_dateFormatter stringFromDate:[_dataSource objectForKey:KLEAVE_BEGINDATE]];
    self.beginTimeView.detailLabel.text = [_timeFormatter stringFromDate:[_dataSource objectForKey:KLEAVE_BEGINDATE]];
    self.endDateView.detailLabel.text = [_dateFormatter stringFromDate:[_dataSource objectForKey:KLEAVE_ENDDATE]];
    self.endTimeView.detailLabel.text = [_timeFormatter stringFromDate:[_dataSource objectForKey:KLEAVE_ENDDATE]];
    self.daysView.detailLabel.text = [_dataSource objectForKey:@"leaveDays"];
    self.typeView.detailLabel.text = [_dataSource objectForKey:KLEAVE_TYPE];
    self.describeView.detailLabel.text = [_dataSource objectForKey:KMODEL_CONTENT];
//    self.describeView.detailLabel.text = @"";
    
    CGFloat height = [_describeView heightWithDetailText:[_dataSource objectForKey:KMODEL_CONTENT]];
    self.describeView.frame = CGRectMake(10, 0, self.tableView.frame.size.width - 20, height);
    
//    [self.tableView reloadData];
}

- (void)configurationViews
{
    NSMutableArray *beginArray = [NSMutableArray arrayWithObjects:self.beginDateView, self.beginTimeView, nil];
    NSMutableArray *endArray = [NSMutableArray arrayWithObjects:self.endDateView, self.endTimeView, nil];
    NSMutableArray *dayArray = [NSMutableArray arrayWithObjects:self.daysView, nil];
    NSMutableArray *typeArray = [NSMutableArray arrayWithObjects:self.typeView, nil];
    NSMutableArray *describeArray = [NSMutableArray arrayWithObjects:self.describeView, nil];
    [_viewsArray addObject:beginArray];
    [_viewsArray addObject:endArray];
    [_viewsArray addObject:dayArray];
    [_viewsArray addObject:typeArray];
    [_viewsArray addObject:describeArray];
}

#pragma mark - action

- (void)doneAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)beginDateAction:(id)sender
{
    
}

- (void)beginTimeAction:(id)sender
{
    
}

- (void)endDateAction:(id)sender
{
    
}

- (void)endTimeAction:(id)sender
{
    
}

- (void)typeAction:(id)sender
{
    
}

- (void)describeAction:(id)sender
{
    XDRemarkViewController *remarkViewController = [[XDRemarkViewController alloc] initWithRemark:self.describeView.textView.text placeholder:@"请输入事由说明..."];
    [self.navigationController pushViewController:remarkViewController animated:YES];
}

@end
