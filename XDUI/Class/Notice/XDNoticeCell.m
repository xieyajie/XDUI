//
//  XDNoticeCell.m
//  XDUI
//
//  Created by xieyajie on 13-11-5.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDNoticeCell.h"

#import "NSDate+Category.h"

@implementation XDNoticeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, KNOTICE_CELL_MIN_HEIGHT - 2 * 10)];
        _mainView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_mainView];
        
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(_mainView.frame.size.width - 10 - 100, 10, 100, 20)];
//        _dateLabel.backgroundColor = [UIColor redColor];
        _dateLabel.font = [UIFont systemFontOfSize:14.0];
        _dateLabel.textColor = [UIColor grayColor];
        _dateLabel.textAlignment = NSTextAlignmentRight;
        [_mainView addSubview:_dateLabel];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, _mainView.frame.size.width - 20 - 100, 20)];
//        _titleLabel.backgroundColor = [UIColor yellowColor];
//        _titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        _titleLabel.textColor = [UIColor blackColor];
        [_mainView addSubview:_titleLabel];
        
        _contentLabel = [[UILabel alloc] init];
//        _contentLabel.backgroundColor = [UIColor redColor];
        _contentLabel.font = [UIFont systemFontOfSize:14.0];
        _contentLabel.textColor = [UIColor grayColor];
        _contentLabel.numberOfLines = 0;
        [_mainView addSubview:_contentLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDate:(NSDate *)date
{
    _dateLabel.text = [date timeIntervalDescription];
}

- (void)setContent:(NSString *)content
{
    _content = content;
    
    CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(280, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    CGRect rect = _mainView.frame;
    if (size.height < 20) {
        _mainView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, KNOTICE_CELL_MIN_HEIGHT);
    }
    else{
        _mainView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, KNOTICE_CELL_MAX_HEIGHT);
    }
    
    _contentLabel.frame = CGRectMake(10, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + 5, _mainView.frame.size.width - 20, _mainView.frame.size.height - (_titleLabel.frame.origin.y + _titleLabel.frame.size.height + 15));
    _contentLabel.text = content;
}

@end
