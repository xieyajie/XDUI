//
//  XDClockDetailViewController.m
//  XDUI
//
//  Created by xieyajie on 13-10-14.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDClockDetailViewController.h"

#import "XDClockDetailCell.h"
#import "XDPublicView.h"

#define KCLOCKQUERY_DUTY_ON @"onDuty"
#define KCLOCKQUERY_DUTY_OFF @"offDuty"
#define KCLOCKQUERY_DUTY_ERROR @"duty_error"
#define KCLOCKQUERY_DUTY_TIME @"duty_time"
#define KCLOCKQUERY_DUTY_IMAGE @"duty_images"
#define KCLOCKQUERY_DUTY_ADDRESS @"duty_address"
#define KCLOCKQUERY_DUTY_REMARK @"duty_remark"
#define KCLOCKQUERY_DUTY_AUDIT @"duty_audit"

#define KCLOCKQUERY_CELLROW_HEADER 0
#define KCLOCKQUERY_CELLROW_ADDRESS 1
#define KCLOCKQUERY_CELLROW_DATE 2
#define KCLOCKQUERY_CELLROW_IMAGE 3
#define KCLOCKQUERY_CELLROW_REMARK 4
#define KCLOCKQUERY_CELLROW_ERROR 5

@interface XDClockDetailViewController ()
{
    NSMutableArray *_dataSource;
    NSDateFormatter *_dateFormatter;
    
    BOOL _onErrorDone;
    BOOL _offErrorDone;
}

@end

@implementation XDClockDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _dataSource = [NSMutableArray array];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm";
        
        _onErrorDone = NO;
        _offErrorDone = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    self.title = @"考勤详情";
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@""] style:UIBarButtonItemStylePlain target:self action:@selector(rightItemAction:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self configurationData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSDictionary *dic = [_dataSource objectAtIndex:section];
    NSInteger errorCode = [[dic objectForKey:KCLOCKQUERY_DUTY_ERROR] integerValue];
    
    switch (errorCode) {
        case -1:
            return 5 + 1;
            break;
        case 0:
            return 4 + 1;
            break;
        case 1:
            return 5 + 1;
            break;
            
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == KCLOCKQUERY_CELLROW_HEADER) {
        static NSString *HeaderCellIdentifier = @"HeaderCell";
        UITableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
        
        // Configure the cell...
        if (headerCell == nil) {
            headerCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
            headerCell.selectionStyle = UITableViewCellSelectionStyleNone;
            headerCell.backgroundColor = [UIColor clearColor];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor grayColor];
            label.font = [UIFont systemFontOfSize:18.0];
            label.tag = 100;
            [headerCell.contentView addSubview:label];
        }
        
        UILabel *headerLabel = (UILabel *)[headerCell.contentView viewWithTag:100];
        if (indexPath.section == 0) {
            headerLabel.text = @"上班";
        }
        else{
            headerLabel.text = @"下班";
        }

        return headerCell;
    }
    
    static NSString *CellIdentifier = @"ClockDetailCell";
    XDClockDetailCell *cell = (XDClockDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[XDClockDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.section];
    NSInteger errorCode = 0;
    if (dic) {
        errorCode = [[dic objectForKey:KCLOCKQUERY_DUTY_ERROR] integerValue];
    }
    
    switch (indexPath.row) {
        case KCLOCKQUERY_CELLROW_ADDRESS:
            [self setupAddressCell:cell info:dic errorCode:errorCode];
            break;
        case KCLOCKQUERY_CELLROW_DATE:
            [self setupDateCell:cell info:dic errorCode:errorCode];
            break;
        case KCLOCKQUERY_CELLROW_IMAGE:
            [self setupImageCell:cell info:dic errorCode:errorCode];
            break;
        case KCLOCKQUERY_CELLROW_REMARK:
            [self setupRemarkCell:cell info:dic errorCode:errorCode];
            break;
        case KCLOCKQUERY_CELLROW_ERROR:
            [self setupErrorCell:cell info:dic errorCode:errorCode];
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = @"";
    switch (indexPath.row) {
        case KCLOCKQUERY_CELLROW_HEADER:
            return 50.0;
            break;
        case KCLOCKQUERY_CELLROW_IMAGE:
        {
            XDPublicView *tmp = [[XDPublicView alloc] init];
            return [tmp heightForImageState] + 10;
        }
            break;
        case KCLOCKQUERY_CELLROW_ADDRESS:
            key = KCLOCKQUERY_DUTY_ADDRESS;
            break;
        case KCLOCKQUERY_CELLROW_REMARK:
            key = KCLOCKQUERY_DUTY_REMARK;
            break;
        default:
            break;
    }
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.section];
    if (dic && key.length > 0) {
        NSString *string = [dic objectForKey:key];
        
        XDPublicView *pv = [[XDPublicView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 20, 40)];
        return [pv heightWithDetailText:string] + 10;
    }

    return 50.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    if (indexPath.row == KCLOCKQUERY_CELLROW_ERROR) {
        //与服务器交互
        //????
        
        //测试数据
        NSMutableDictionary *dic = [_dataSource objectAtIndex:indexPath.section];
        NSInteger errorCode = [[dic objectForKey:KCLOCKQUERY_DUTY_ERROR] integerValue];
        if (errorCode < 0) {
            [dic setObject:[NSNumber numberWithInteger:1] forKey:KCLOCKQUERY_DUTY_ERROR];
        }
        
        [self.tableView beginUpdates];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

#pragma mark - private setup cell

- (void)setupAddressCell:(XDClockDetailCell *)aCell info:(NSDictionary *)info errorCode:(NSInteger)code
{
    aCell.title = @"位置";
    
    NSString *string = [info objectForKey:KCLOCKQUERY_DUTY_ADDRESS];
    if (string == nil) {
        string = @"";
    }
    
    aCell.detailText = string;
    if (code < 0) {
        aCell.textColor = [UIColor redColor];
    }
    else{
        aCell.textColor = [UIColor blackColor];
    }
}

- (void)setupDateCell:(XDClockDetailCell *)aCell info:(NSDictionary *)info errorCode:(NSInteger)code
{
    aCell.title = @"时间";
    
    NSString *string = [_dateFormatter stringFromDate:[info objectForKey:KCLOCKQUERY_DUTY_TIME]];
    if (string == nil) {
        string = @"";
    }
    
    aCell.detailText = string;
    if (code < 0) {
        aCell.textColor = [UIColor redColor];
    }
    else{
        aCell.textColor = [UIColor blackColor];
    }
}

- (void)setupImageCell:(XDClockDetailCell *)aCell info:(NSDictionary *)info errorCode:(NSInteger)code
{
    aCell.title = @"照片";
    [aCell setupWithImageSource:[info objectForKey:KCLOCKQUERY_DUTY_IMAGE] canEdit:NO];
}

- (void)setupRemarkCell:(XDClockDetailCell *)aCell info:(NSDictionary *)info errorCode:(NSInteger)code
{
    aCell.title = @"备注";
    
    NSString *string = [info objectForKey:KCLOCKQUERY_DUTY_REMARK];
    if (string == nil) {
        string = @"";
    }
    
    aCell.detailText = string;
    if (code < 0) {
        aCell.textColor = [UIColor redColor];
    }
    else{
        aCell.textColor = [UIColor blackColor];
    }
}

- (void)setupErrorCell:(XDClockDetailCell *)aCell info:(NSDictionary *)info errorCode:(NSInteger)code
{
    if (code < 0) {
        [aCell showButtonToTableView:self.tableView title:@"确认异常"];
    }
    else if (code > 0)
    {
        aCell.title = @"确认异常";
        aCell.detailText = @"李丽确认异常";
        aCell.textColor = [UIColor greenColor];
    }
}

#pragma mark - data

- (void)configurationData
{
    //从服务器获取信息
    //????
    
    //测试数据
    NSArray *images1 = [NSArray array];
    NSMutableDictionary *dic1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:-1], KCLOCKQUERY_DUTY_ERROR, @"北京市海淀区海淀西大街48号图书城步行街内鑫鼎宾馆", KCLOCKQUERY_DUTY_ADDRESS, [NSDate date], KCLOCKQUERY_DUTY_TIME, images1, KCLOCKQUERY_DUTY_IMAGE, @"上午拜访客户", KCLOCKQUERY_DUTY_REMARK, nil];
    [_dataSource addObject:dic1];
    
    NSArray *images2 = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"clock_addImage.png"], [UIImage imageNamed:@"clock_addImage.png"], nil];
    NSMutableDictionary *dic2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:0], KCLOCKQUERY_DUTY_ERROR,@"北京市海淀区海淀西大街48号图书城步行街内鑫鼎宾馆", KCLOCKQUERY_DUTY_ADDRESS, [NSDate date], KCLOCKQUERY_DUTY_TIME, images2, KCLOCKQUERY_DUTY_IMAGE, @"", KCLOCKQUERY_DUTY_REMARK, nil];
    [_dataSource addObject:dic2];
    
    [self.tableView reloadData];
}

#pragma mark - item/button action

- (void)rightItemAction:(id)sender
{
    
}

@end
