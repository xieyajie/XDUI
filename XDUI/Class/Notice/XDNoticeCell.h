//
//  XDNoticeCell.h
//  XDUI
//
//  Created by xieyajie on 13-11-5.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XDNoticeCell : UITableViewCell
{
    UIView *_mainView;
    UILabel *_dateLabel;
    UILabel *_contentLabel;
}

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSDate *date;

@end
