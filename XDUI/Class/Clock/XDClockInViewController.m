//
//  XDClockInViewController.m
//  XDUI
//
//  Created by xie yajie on 13-10-12.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDClockInViewController.h"

#import "XDPublicView.h"
#import "XDRemarkViewController.h"

@interface XDClockInViewController ()<XDPublicImageViewDelegate>
{
    UIBarButtonItem *_doneItem;
    NSDateFormatter *_dateFormatter;
    
    NSDate *_selectedDate;
    BOOL _canEdit;
    
    NSMutableArray *_imageSource;
}

@property (strong, nonatomic) XDPublicView *addressView;
@property (strong, nonatomic) XDPublicView *dateView;
@property (strong, nonatomic) XDPublicView *imageView;
@property (strong, nonatomic) XDPublicView *remarkView;

@property (strong, nonatomic) NSString *addressString;
@property (strong, nonatomic) NSString *remarkString;

@end

@implementation XDClockInViewController

@synthesize addressView = _addressView;
@synthesize dateView = _dateView;
@synthesize imageView = _imageView;
@synthesize remarkView = _remarkView;

@synthesize addressString = _addressString;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _imageSource = [NSMutableArray array];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm";
        
        _addressString = @"";
        _selectedDate = [NSDate date];
        _remarkString = @"";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
 
    _doneItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"doneButtonImage.png"] style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
    self.navigationItem.rightBarButtonItem = _doneItem;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remarkEditFinish:) name:@"remarkEditFinish" object:nil];
    
    _addressString = [self selfAddress];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (XDPublicView *)addressView
{
    if (_addressView == nil) {
        _addressView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _addressView.titleLabel.text = @"当前位置";
        
        UIButton *addressButton = [[UIButton alloc] init];
        [addressButton setImage:[UIImage imageNamed:@"refresh.png"] forState:UIControlStateNormal];
        [addressButton setImage:[UIImage imageNamed:@"refresh_highlighted.png"] forState:UIControlStateHighlighted];
        [addressButton addTarget:self action:@selector(addressAction:) forControlEvents:UIControlEventTouchUpInside];
        _addressView.rightAccessoryView = addressButton;
    }
    
    return _addressView;
}

- (XDPublicView *)dateView
{
    if (_dateView == nil) {
        _dateView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _dateView.titleLabel.text = @"时间";
        [_dateView addTarget:self action:@selector(dateAction:)];
    }
    
    return _dateView;
}

- (XDPublicView *)imageView
{
    if (_imageView == nil) {
        _imageView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 80) style:XDPublicViewStyleImage];
        _imageView.titleLabel.text = @"照片";
        
        _imageView.imageDelegate = self;
        [_imageView addTarget:self action:@selector(dateAction:)];
    }
    
    return _imageView;
}

- (XDPublicView *)remarkView
{
    if (_remarkView == nil) {
        _remarkView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleText];
        _remarkView.titleLabel.text = @"备注";
        _remarkView.textView.placeholder = @"请输入备注...";
        
        [_remarkView addTarget:self action:@selector(remarkAction:)];
        [_remarkView setupForRemarkWithViewJump:YES];
    }
    
    return _remarkView;
}

- (NSString *)addressString
{
    if (_addressString == nil) {
        _addressString = [self selfAddress];
    }
    
    return _addressString;
}

#pragma mark - setting

- (void)setAddressString:(NSString *)string
{
    _addressString = string;
    self.addressView.detailLabel.text = _addressString;
}

- (void)setRemarkString:(NSString *)string
{
    _remarkString = string;
    self.remarkView.textView.text = _remarkString;
}

#pragma mark - notifation

- (void)remarkEditFinish:(NSNotification *)notification
{
    if ([notification.object isKindOfClass:[NSString class]]) {
        self.remarkString = (NSString *)notification.object;
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:3 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else
    {
        for (UIView *subview in cell.contentView.subviews) {
            [subview removeFromSuperview];
        }
    }
    
    switch (indexPath.row) {
        case 0:
            [cell.contentView addSubview:self.addressView];
            self.addressView.detailLabel.text = self.addressString;
            break;
        case 1:
            [cell.contentView addSubview:self.dateView];
            self.dateView.detailLabel.text = [_dateFormatter stringFromDate:_selectedDate];
            break;
        case 2:
            [cell.contentView addSubview:self.imageView];
            [self.imageView setupWithImageSource:_imageSource canEdit:YES];
            break;
        case 3:
            [cell.contentView addSubview:self.remarkView];
            self.remarkView.textView.text = _remarkString;
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    switch (indexPath.row) {
        case 0:
            height = [self.addressView heightWithDetailText:_addressString] + 20;
            self.addressView.frame = CGRectMake(10, 10, self.view.frame.size.width - 20, height - 20);
            break;
        case 1:
            height = 60.0;
            self.dateView.frame = CGRectMake(10, 10, self.view.frame.size.width - 20, height - 20);
            break;
        case 2:
            height = [self.imageView heightForImageState] + 20;
            self.imageView.frame = CGRectMake(10, 10, self.view.frame.size.width - 20, height - 20);
            break;
        case 3:
            height = [self.remarkView heightForTextViewWithString:_remarkString] + 20;
            self.remarkView.frame = CGRectMake(10, 10, self.view.frame.size.width - 20, height - 20);
            break;
            
        default:
            height = 0.0;
            break;
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

#pragma mark - XDPublicImageViewDelegate

- (void)publicImageViewDidAddImage:(XDPublicView *)publicView
{
    //添加图片调用
}

- (void)publicImageView:(XDPublicView *)publicView didDeleteAtIndex:(NSInteger)index
{
    //删除图片调用
}

#pragma mark - private

#pragma mark - item/button action

- (void)doneAction:(id)sender
{
    
}

- (void)addressAction:(id)sender
{
    _addressString = nil;
    self.addressView.detailLabel.text = self.addressString;
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

- (void)dateAction:(id)sender
{
    
}

- (void)remarkAction:(id)sender
{
    XDRemarkViewController *remarkViewController = [[XDRemarkViewController alloc] initWithRemark:self.remarkView.textView.text placeholder:@"请输入备注..."];
    [self.navigationController pushViewController:remarkViewController animated:YES];
}

#pragma mark - public

- (NSString *)selfAddress
{
    return @"北京市海淀区海淀西大街48号图书城步行街内鑫鼎宾馆北京市海淀区海淀西大街48号图书城步行街内鑫鼎宾馆";
}

- (void)configurationDataWithDutyState:(XDClockDutyState)state
{
    //从服务器获取数据
    //????
    
    //测试数据
    [_imageSource addObject:[UIImage imageNamed:@"clock_addImage.png"]];
    [_imageSource addObject:[UIImage imageNamed:@"clock_addImage.png"]];
    [_imageSource addObject:[UIImage imageNamed:@"clock_addImage.png"]];
    [_imageSource addObject:[UIImage imageNamed:@"clock_addImage.png"]];
    [_imageSource addObject:[UIImage imageNamed:@"clock_addImage.png"]];
    
    _addressString = @"北京市海淀区海淀西大街48号图书城步行街内鑫鼎宾馆北京市海淀区海淀西大街48号图书城步行街内鑫鼎宾馆";
    _selectedDate = [NSDate date];
    _remarkString = @"";
    
    [self.tableView reloadData];
}

@end
