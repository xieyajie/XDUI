//
//  XDAppendixView.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XDAppendixModel.h"

typedef enum {
    XDMediaViewLayoutStyleThumbnail,
    XDMediaViewLayoutStyleDetail,
    XDMediaViewLayoutStyleScaleToFill
} XDMediaViewLayoutStyle;

@interface XDAppendixView : UIView

@property (nonatomic, strong) NSArray *medias;        //多媒体数组
@property (nonatomic) XDMediaViewLayoutStyle layoutStyle;    //布局模式

- (id)initWithFrame:(CGRect)frame media:(XDAppendixModel *)media layoutStyle:(XDMediaViewLayoutStyle)layoutStyle;
- (id)initWithFrame:(CGRect)frame medias:(NSArray *)medias layoutStyle:(XDMediaViewLayoutStyle)layoutStyle;

+ (CGFloat)heightWithFrame:(CGRect)frame media:(XDAppendixModel *)media layoutStyle:(XDMediaViewLayoutStyle)layoutStyle;
+ (CGFloat)heightWithFrame:(CGRect)frame medias:(NSArray *)medias layoutStyle:(XDMediaViewLayoutStyle)layoutStyle;

@end
