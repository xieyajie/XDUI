//
//  XDAuditModel.h
//  XDUI
//
//  Created by xie yajie on 13-10-26.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDModel.h"

@interface XDAuditModel : XDModel

@property (nonatomic) NSInteger state;//动态的审批意见类型
@property (nonatomic, copy) NSString *stateDescription;//动态的审批意见描述

@end
