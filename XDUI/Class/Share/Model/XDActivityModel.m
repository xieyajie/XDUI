//
//  XDActivityModel.m
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDActivityModel.h"

#import "XDAuditModel.h"
#import "XDCommentModel.h"
#import "XDAppendixModel.h"

#import "NSMutableArray+Category.h"
#import "NSString+Category.h"
#import "RegexKitLite.h"

@implementation XDActivityModel

@synthesize finance = _finance;
@synthesize stateDescription = _stateDescription;

@synthesize auditList = _auditList, commentList = _commentList;

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.ID = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ID] integerValue]];
        self.accountId = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ACCOUNTID] integerValue]];
        self.name = [dictionary objectForKey:KMODEL_NAME];
        self.headImagePath = [dictionary objectForKey:KMODEL_HEADPATH];
//        self.gender = [dictionary objectForKey:KMODEL_GENDER];
        self.createDate = [dictionary objectForKey:KMODEL_CREATEDATE];
        self.address = [dictionary objectForKey:KMODEL_ADDRESS];
//        self.coordinate = CLLocationCoordinate2DMake([[dictionary objectForKey:KMODEL_LATITUDE] doubleValue], [[dictionary objectForKey:KMODEL_LONGITUDE] doubleValue]);
        
        //动态状态
        NSInteger stateCode = [[dictionary objectForKey:KMODEL_STATE] integerValue];
        self.stateDescription = [self descriptionForState:stateCode];
        
        //内容
        self.title = [dictionary objectForKey:KMODEL_TITLE];
        
        NSString *money = [dictionary objectForKey:KACTIVITY_FINANCE];
        if (money == nil || [money length] == 0) {
            self.finance = @"";
        }
        else{
            self.finance = [NSString stringWithFormat:@"￥%@", money];
        }
        
        self.content = [dictionary objectForKey:KMODEL_CONTENT];
        
        //附件列表
        self.isHaveAppendix = NO;
        
        NSString *regexString = @"\\[心情:.+?\\]";
        NSString *matchedString = [self.content stringByMatching:regexString];
        if (matchedString.length) {//心情
            self.content = [[self.content stringByReplacingOccurrencesOfRegex:regexString withString:@""] trimmedString];
            self.isHaveAppendix = YES;
        }
        
        NSDictionary *appendixDic = [dictionary objectForKey:KAPPENDIX];
        if ([appendixDic count] != 0) {
            [self setupAppendixWithDictionary:appendixDic];
            self.isHaveAppendix = YES;
        }
        
        //审批列表
        NSArray *audits = [dictionary objectForKey:KMODEL_AUDITLIST];
        if (audits == nil || [audits count] == 0) {
            self.isHaveAudit = NO;
        }
        else{
            self.isHaveAudit = YES;
            if (_auditList == nil) {
                _auditList = [NSMutableArray array];
            }
            else{
                [_auditList removeAllObjects];
            }
            
            for (NSDictionary *auditDictionary in audits) {
                XDAuditModel *auditModel = [[XDAuditModel alloc] initWithDictionary:auditDictionary];
                [self.auditList addObject:auditModel];
            }
            [self.auditList sortWithKey:KMODEL_CREATEDATE ascending:YES selector:@selector(compare:)];
        }
        
        //评论列表
        NSArray *comments = [dictionary objectForKey:KMODEL_COMMENTLIST];
        if (comments == nil || [comments count] == 0) {
            self.isHaveComment = NO;
        }
        else{
            self.isHaveComment = YES;
            if (_commentList == nil) {
                _commentList = [NSMutableArray array];
            }
            else{
                [_commentList removeAllObjects];
            }
            
            for (NSDictionary *commentDictionary in comments) {
                XDCommentModel *commentModel = [[XDCommentModel alloc] initWithDictionary:commentDictionary];
                [self.commentList addObject:commentModel];
            }

//            [self.commentList sortWithKey:KMODEL_CREATEDATE ascending:YES selector:@selector(compare:)];
        }
    }
    
    return self;
}

- (NSString *)descriptionForState:(NSInteger)code
{
    switch (code) {
        case 0:
            return @"审批 - 审批中";
            break;
        case 1:
            return @"已审批";
            break;
            
        default:
            return @"";
            break;
    }
}

- (NSArray *)appendixModelsForArray:(NSArray *)array
{
    NSMutableArray *results = [NSMutableArray array];
    
    if (array == nil || [array count] == 0) {
        return results;
    }
    
    for (NSDictionary *dic in array) {
        XDAppendixModel *appendixModel = [[XDAppendixModel alloc] initWithDictionary:dic];
        if (appendixModel != nil) {
            [results addObject:appendixModel];
        }
    }
    
    return results;
}

- (void)setupAppendixWithDictionary:(NSDictionary *)dictionary
{
    if (!self.appendixDictionary) {
        self.appendixDictionary = [[NSMutableDictionary alloc] init];
    }
    else{
        [self.appendixDictionary removeAllObjects];
    }
    
    NSArray *images = [dictionary objectForKey:KAPPENDIX_IMAGE];
    if (images && [images count] > 0) {
        [self.appendixDictionary setObject:[self appendixModelsForArray:images] forKey:[NSNumber numberWithInteger:XDMediaTypeImage]];
    }
    
    NSArray *videos = [dictionary objectForKey:KAPPENDIX_VIDEO];
    if (videos && [videos count] > 0) {
        [self.appendixDictionary setObject:[self appendixModelsForArray:videos] forKey:[NSNumber numberWithInteger:XDMediaTypeVideo]];
    }
    
    NSArray *audios = [dictionary objectForKey:KAPPENDIX_AUDIO];
    if (audios && [audios count] > 0) {
        [self.appendixDictionary setObject:[self appendixModelsForArray:audios] forKey:[NSNumber numberWithInteger:XDMediaTypeAudio]];
    }
}


@end
