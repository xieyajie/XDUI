//
//  XDLeaveQueryViewController.m
//  XDUI
//
//  Created by xieyajie on 13-11-7.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDLeaveQueryViewController.h"

#import "XDLeaveQueryCell.h"
#import "XDLeaveOperateViewController.h"

@interface XDLeaveQueryViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *_dataSource;
    
    NSDateFormatter *_dateFormatter;
}

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UITableView *tableView;

@end

@implementation XDLeaveQueryViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        _dataSource = [NSMutableArray array];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"请假查询";
    
    [self.view addSubview:self.headerView];
    
    self.tableView.frame = CGRectMake(0, self.headerView.frame.origin.y + self.headerView.frame.size.height, self.view.frame.size.width, self.sizeHeight - (self.headerView.frame.origin.y + self.headerView.frame.size.height));
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
    [self.view addSubview:self.tableView];
    
    [self configurationData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (UIView *)headerView
{
    if (_headerView == nil) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.originY, self.view.frame.size.width, 40.0)];
        _headerView.backgroundColor = [UIColor colorWithRed:237 / 255.0 green:237 / 255.0 blue:237 / 255.0 alpha:1.0];
        
        UIButton *dateButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, _headerView.frame.size.width / 2 - 1, _headerView.frame.size.height - 1)];
        [dateButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [dateButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [dateButton setImage:[UIImage imageNamed:@"arrowDown.png"] forState:UIControlStateNormal];
        [dateButton setImage:[UIImage imageNamed:@"arrowUp.png"] forState:UIControlStateSelected];
        [dateButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
        [dateButton setImageEdgeInsets:UIEdgeInsetsMake(0, 105, 0, 30)];
        [dateButton setTitle:@"所有日期" forState:UIControlStateNormal];
        [dateButton addTarget:self action:@selector(headerDateAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:dateButton];
        
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(_headerView.frame.size.width / 2 - 1, 0, 1, _headerView.frame.size.height)];
        line1.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
        [_headerView addSubview:line1];
        
        UIButton *employeeButton = [[UIButton alloc] initWithFrame:CGRectMake(_headerView.frame.size.width / 2, 0, _headerView.frame.size.width / 2, _headerView.frame.size.height - 1)];
        [employeeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [employeeButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [employeeButton setImage:[UIImage imageNamed:@"arrowDown.png"] forState:UIControlStateNormal];
        [employeeButton setImage:[UIImage imageNamed:@"arrowUp.png"] forState:UIControlStateSelected];
        [employeeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
        [employeeButton setImageEdgeInsets:UIEdgeInsetsMake(0, 105, 0, 30)];
        [employeeButton setTitle:@"所有员工" forState:UIControlStateNormal];
        [employeeButton addTarget:self action:@selector(headerEmployeeAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:employeeButton];
        
        UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, _headerView.frame.size.height - 1, _headerView.frame.size.width, 1)];
        line2.backgroundColor = [UIColor colorWithRed:220 / 255.0 green:221 / 255.0 blue:221 / 255.0 alpha:1.0];
        [_headerView addSubview:line2];
    }
    
    return _headerView;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    
    return _tableView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LeaveQueryCell";
    XDLeaveQueryCell *cell = (XDLeaveQueryCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[XDLeaveQueryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    if (dic) {
        cell.headerImageName = [dic objectForKey:KMODEL_HEADPATH];
        cell.name = [dic objectForKey:KMODEL_NAME];
        cell.content = [dic objectForKey:KMODEL_CONTENT];
        
        NSString *fromStr = [_dateFormatter stringFromDate:[dic objectForKey:KLEAVE_BEGINDATE]];
        NSString *toStr = [_dateFormatter stringFromDate:[dic objectForKey:KLEAVE_ENDDATE]];
        cell.dateString = [NSString stringWithFormat:@"%@ 到 %@", fromStr, toStr];
        
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    XDLeaveOperateViewController *operateVC = [[XDLeaveOperateViewController alloc] initDetailWithStyle:UITableViewStylePlain withInfo:nil];
    operateVC.title = @"请假详情";
    [self.navigationController pushViewController:operateVC animated:YES];
}

#pragma mark - data

- (void)configurationData
{
    //服务器获取数据
    //????
    
    //测试数据
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"clock_headerImageDefult.png", KMODEL_HEADPATH, @"Anne", KMODEL_NAME, @"感冒发烧请假一天", KMODEL_CONTENT, [NSDate date], KLEAVE_BEGINDATE, [NSDate date], KLEAVE_ENDDATE, nil];
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"clock_headerImageDefult.png", KMODEL_HEADPATH, @"白鹭", KMODEL_NAME, @"感冒发烧请假一天", KMODEL_CONTENT, [NSDate date], KLEAVE_BEGINDATE, [NSDate date], KLEAVE_ENDDATE, nil];
    
    for (int i = 0; i < 5; i++) {
        [_dataSource addObject:dic1];
        [_dataSource addObject:dic2];
    }
    
    [self.tableView reloadData];
}

#pragma mark - button/item action

- (void)headerDateAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
}

- (void)headerEmployeeAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
}

@end
