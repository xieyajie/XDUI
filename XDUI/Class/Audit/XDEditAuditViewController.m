//
//  XDEditAuditViewController.m
//  XDUI
//
//  Created by xieyajie on 13-10-24.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDEditAuditViewController.h"

#import "XDPublicView.h"
#import "XDRemarkViewController.h"

@interface XDEditAuditViewController ()<XDPublicImageViewDelegate>
{
    NSMutableArray *_imageSource;
    XDAuditState _state;
}

@property (strong, nonatomic) XDPublicView *personView;//选择审批人
@property (strong, nonatomic) XDPublicView *rangeView;//抄送范围
@property (strong, nonatomic) XDPublicView *imageView;//照片
@property (strong, nonatomic) XDPublicView *opinionView;//审批意见

@end

@implementation XDEditAuditViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        // Custom initialization
        _imageSource = [NSMutableArray array];
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style auditState:(XDAuditState)state
{
    self = [self initWithStyle:UITableViewStylePlain];
    if (self) {
        // Custom initialization
        _state = state;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    self.title = @"处理审批";
 
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"doneButtonImage.png"] style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (XDPublicView *)personView
{
    if (_personView == nil) {
        _personView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _personView.titleLabel.text = @"下一审批人";
        _personView.detailLabel.textAlignment = NSTextAlignmentRight;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        imageView.image = [UIImage imageNamed:@""];
        _personView.rightAccessoryView = imageView;
    }
    
    return _personView;
}

- (XDPublicView *)rangeView
{
    if (_rangeView == nil) {
        _rangeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 1, self.view.frame.size.width - 20, 39) style:XDPublicViewStyleDetail];
        _rangeView.titleLabel.text = @"抄送范围";
        _rangeView.detailLabel.textAlignment = NSTextAlignmentRight;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        imageView.image = [UIImage imageNamed:@""];
        _rangeView.rightAccessoryView = imageView;
    }
    
    return _rangeView;
}

- (XDPublicView *)imageView
{
    if (_imageView == nil) {
        _imageView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 80) style:XDPublicViewStyleImage];
        _imageView.titleLabel.text = @"照片";
        _imageView.imageDelegate = self;
        [_imageView setupWithImageSource:_imageSource canEdit:YES];
    }
    
    return _imageView;
}

- (XDPublicView *)opinionView
{
    if (_opinionView == nil) {
        _opinionView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 100) style:XDPublicViewStyleText];
        _opinionView.titleLabel.text = @"审批意见";
        _opinionView.textView.placeholder = @"请输入审批意见...";
        
        [_opinionView addTarget:self action:@selector(opinionAction:)];
        [_opinionView setupForRemarkWithViewJump:YES];
    }
    
    return _opinionView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        if (_state == XDAuditStateDisagree) {
            return 2;
        }
        else{
            return 0;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    NSInteger row = indexPath.row;
    switch (indexPath.section) {
        case 0:
        {
            if (row == 0) {
                self.personView.detailLabel.text = @"陈军";
                [cell.contentView addSubview:self.personView];
            }
            else if (row == 1) {
                self.rangeView.detailLabel.text = @"Avery，白露，陈军";
                [cell.contentView addSubview:self.rangeView];
            }
        }
            break;
        case 1:
        {
            if (row == 0) {
                [cell.contentView addSubview:self.imageView];
            }
        }
            break;
        case 2:
        {
            if (row == 0) {
                [cell.contentView addSubview:self.opinionView];
            }
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 && _state != XDAuditStateDisagree) {
        return 0;
    }
    else{
        return 20.0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 1:
            return [self.imageView heightForImageState];
            break;
        case 2:
            return 100;//????
            break;
            
        default:
            break;
    }
    
    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            //????
        }
        else if (indexPath.row == 1)
        {
            //????
        }
    }
}

#pragma mark - XDPublicImageViewDelegate

- (void)publicImageViewDidAddImage:(XDPublicView *)publicView
{
    //添加图片调用
}

- (void)publicImageView:(XDPublicView *)publicView didDeleteAtIndex:(NSInteger)index
{
    //删除图片调用
}

#pragma mark - button/item action

- (void)doneAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)opinionAction:(id)sender
{
    XDRemarkViewController *remarkViewController = [[XDRemarkViewController alloc] initWithRemark:self.opinionView.textView.text placeholder:@"请输入审批意见..."];
    [self.navigationController pushViewController:remarkViewController animated:YES];
}

@end
