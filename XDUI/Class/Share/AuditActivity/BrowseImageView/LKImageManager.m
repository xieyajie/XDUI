//
//  LKImageManager.m
//  Travel
//
//  Created by tixa tixa on 13-4-23.
//  Copyright (c) 2013年 tixa. All rights reserved.
//

#import "LKImageManager.h"
#import "LKScrollView.h"

@interface LKImageManager ()
{
    int _pageNum;
    UIScrollView *scrollView;
    NSArray *_scrollList;
    
    LKScrollView *_selectedScrollView;
}
@end

@implementation LKImageManager

- (id)init
{
    self = [super init];
    if (self) {
        _column = 4;
        _edge = 15;
        _corner = 6.0;
    }
    return self;
}

- (void)showWithScrollList:(NSArray *)scrollList
{
    _selectedScrollView = nil;
    
    _scrollList = scrollList;
    _pageNum = scrollList.count;
    CGSize mainSize = [UIApplication sharedApplication].keyWindow.bounds.size;
    float width = mainSize.width + 20;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, width, mainSize.height)];
    scrollView.contentSize = CGSizeMake(_pageNum * width, mainSize.height);
    scrollView.pagingEnabled = YES;
    scrollView.backgroundColor = [UIColor blackColor];
    scrollView.delegate = self;
    
    for (int i = 0; i < _pageNum; i++) {
        id obj = [scrollList objectAtIndex:i];
        if ([obj isKindOfClass:[LKScrollView class]]) {
            LKScrollView *subView = (LKScrollView *)obj;
            CGRect frame = subView.frame;
            frame.origin.x = i * width;
            subView.frame = frame;
            subView.manager = self;
            subView.tag = i;
            [scrollView addSubview:subView];
            [subView enterFullScreen:(i == _touchIndex)];
        }
    }
    
    [[UIApplication sharedApplication].keyWindow addSubview:scrollView];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self scrollToIndex:_touchIndex];
}

// 滚动到指定的位置
- (void)scrollToIndex:(int)index
{
    [self updateSubScrollView:index];
    [scrollView scrollRectToVisible:CGRectMake(index * scrollView.frame.size.width + scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self updatePager];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView1
{
    [self updatePager];
}

- (void)updatePager
{
    int index = floorf(scrollView.contentOffset.x / scrollView.frame.size.width);
    
    [self updateSubScrollView:index];
}

// 根据index 更新子滚动视图
- (void)updateSubScrollView:(int)index
{
    if (index >= 0 && index < _scrollList.count) {
        LKScrollView *subview = [_scrollList objectAtIndex:index];
        if (_selectedScrollView == subview) {
            return;
        }
        
        [_selectedScrollView recoverDoubleView];//将上次放大的图片 缩放回原来大小
        _selectedScrollView = subview;
        if (![subview isLoading]) { //下载完成 则加载图片
            [subview loadImage];
        }
    }
}

- (void)removeScrollView
{
    _selectedScrollView = nil;
    _scrollList = nil;
    _pageNum = 0;
    
    scrollView.delegate = nil;
    [scrollView removeFromSuperview];
    scrollView = nil;
}

@end
