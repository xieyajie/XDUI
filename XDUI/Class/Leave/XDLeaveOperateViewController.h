//
//  XDLeaveOperateViewController.h
//  XDUI
//
//  Created by xieyajie on 13-11-7.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XDLeaveOperateViewController : UITableViewController

- (id)initDetailWithStyle:(UITableViewStyle)style withInfo:(NSDictionary *)info;

@end
