//
//  LKImageView.h
//  Travel
//
//  Created by tixa tixa on 13-4-23.
//  Copyright (c) 2013年 tixa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LKImageView : UIImageView

@property (nonatomic) BOOL thumbMode; // 缩略图 还是 大图    默认NO（缩略图）
@property (nonatomic) BOOL showBorder; // 显示边框  默认NO
@property (nonatomic, strong) UIImage *borderImage;//边框图片

@property (nonatomic, strong) UIImage *normalImage;//中图
@property (nonatomic, strong) NSString *normalURL;//中图路径

@property (nonatomic, strong) UIImage *originalImage;//原图
@property (nonatomic, strong) NSString *originalURL;//原图路径

@property (nonatomic, weak) id parent;
@property (nonatomic) SEL action;

@property (nonatomic) CGPoint touchPoint;    //触摸点


@end
