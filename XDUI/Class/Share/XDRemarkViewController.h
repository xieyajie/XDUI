//
//  XDRemarkViewController.h
//  XDUI
//
//  Created by xie yajie on 13-10-21.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDViewController.h"

@interface XDRemarkViewController : XDViewController

- (id)initWithRemark:(NSString *)text placeholder:(NSString *)placeholder;

@end
