//
//  XDClockViewController.m
//  XDUI
//
//  Created by xie yajie on 13-10-12.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDClockViewController.h"

#import "XDPublicView.h"
#import "XDClockInViewController.h"
#import "XDClockQueryViewController.h"

#define KDATA_ONDUTYSTATE @"onDutyState"
#define KDATA_OFFDUTYSTATE @"offDutyState"

@interface XDClockViewController ()
{
    XDPublicView *_onDutyView;
    XDPublicView *_offDutyView;
    XDPublicView *_searchView;
    
    NSMutableDictionary *_dataSource;
}

@end

@implementation XDClockViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"考勤打卡";
    self.view.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollEnabled = NO;
    
    _onDutyView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleDefault];
    [_onDutyView addTarget:self action:@selector(onDutyAction:)];
    _onDutyView.titleLabel.text = @"上班打卡";
    
    _offDutyView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 1, self.view.frame.size.width - 20, 39) style:XDPublicViewStyleDefault];
    [_offDutyView addTarget:self action:@selector(offDutyAction:)];
    _offDutyView.titleLabel.text = @"下班打卡";
    
    _searchView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleDefault];
    [_searchView addTarget:self action:@selector(searchAction:)];
    _searchView.titleLabel.text = @"考勤查询";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configurationData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 1;
            break;
            
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        for (UIView *subview in cell.contentView.subviews) {
            [subview removeFromSuperview];
        }
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell.contentView addSubview:_onDutyView];
        }
        else if (indexPath.row == 1)
        {
            [cell.contentView addSubview:_offDutyView];
        }
    }
    else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [cell.contentView addSubview:_searchView];
        }
    }
        
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 40;
            break;
        case 1:
            return 20;
            break;
            
        default:
            break;
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.text = @"      考勤时间：08：30-17：30";
        headerLabel.textColor = [UIColor grayColor];
        headerLabel.font = [UIFont systemFontOfSize:14.0];
        return headerLabel;
    }
    else{
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
        label.backgroundColor = [UIColor clearColor];
        return label;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    
    // ...
    // Pass the selected object to the new view controller.
    
}

#pragma mark - data

- (void)configurationData
{
    //从服务器获取数据
    _dataSource = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:0], KDATA_ONDUTYSTATE, [NSNumber numberWithInteger:1], KDATA_OFFDUTYSTATE, nil];
    //end
    
    UIButton *onDutyButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [onDutyButton setImage:[self imageForStateCode:[[_dataSource objectForKey:KDATA_ONDUTYSTATE] integerValue]] forState:UIControlStateNormal];
    [onDutyButton addTarget:self action:@selector(onDutyAction:) forControlEvents:UIControlEventTouchUpInside];
    _onDutyView.leftAccessoryView = onDutyButton;
    
    UIButton *offDutyButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [offDutyButton setImage:[self imageForStateCode:[[_dataSource objectForKey:KDATA_OFFDUTYSTATE] integerValue]] forState:UIControlStateNormal];
    [offDutyButton addTarget:self action:@selector(offDutyAction:) forControlEvents:UIControlEventTouchUpInside];
    _offDutyView.leftAccessoryView = offDutyButton;
    
    [_offDutyView clearContentView];
    
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_offDutyView.contentView.frame.size.width - 50, 0, 40, _offDutyView.contentView.frame.size.height)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.textColor = [UIColor grayColor];
    timeLabel.font = [UIFont systemFontOfSize:12.0];
    timeLabel.textAlignment = NSTextAlignmentRight;
    timeLabel.text = @"18:40";
    [_offDutyView.contentView addSubview:timeLabel];
    
    UILabel *errorLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeLabel.frame.origin.x - 100, 0, 100, _offDutyView.contentView.frame.size.height)];
    errorLabel.backgroundColor = [UIColor clearColor];
    errorLabel.textColor = [UIColor redColor];
    errorLabel.font = [UIFont systemFontOfSize:12.0];
    errorLabel.textAlignment = NSTextAlignmentRight;
    errorLabel.text = @"位置异常";
    [_offDutyView.contentView addSubview:errorLabel];
    
    [self.tableView reloadData];
}

#pragma mark - private

- (UIImage *)imageForStateCode:(NSInteger)code
{
    switch (code) {
        case -1:
            return [UIImage imageNamed:@"clock_error.png"];
            break;
        case 0:
            return [UIImage imageNamed:@"clock_no.png"];
            break;
        case 1:
            return [UIImage imageNamed:@"clock_succeed.png"];
            break;
            
        default:
            return nil;
            break;
    }
}

- (void)pushViewControllerForStateCode:(NSInteger)code dutyState:(XDClockDutyState)dutyState completion: (void (^)(UIViewController *viewController))completion
{
    XDClockInViewController *newClockVC = [[XDClockInViewController alloc] initWithStyle:UITableViewStylePlain];
    
    if (dutyState == XDClockDutyStateOn) {
        newClockVC.title = @"上班打卡";
    }
    else if (XDClockDutyStateOff)
    {
        newClockVC.title = @"下班打卡";
    }
    
    switch (code) {
        case -1:
        {
            [newClockVC configurationDataWithDutyState:dutyState];
        }
            break;
        case 0:
            break;
        case 1:
        {
            [newClockVC configurationDataWithDutyState:dutyState];
        }
            break;
            
        default:
            break;
    }
    
    [self.navigationController pushViewController:newClockVC animated:YES];
    completion(newClockVC);
}

#pragma mark - action

- (void)onDutyAction:(id)sender
{
    [self pushViewControllerForStateCode:[[_dataSource objectForKey:KDATA_ONDUTYSTATE] integerValue] dutyState:XDClockDutyStateOn completion:^(UIViewController *viewController){}];
}

- (void)offDutyAction:(id)sender
{
    [self pushViewControllerForStateCode:[[_dataSource objectForKey:KDATA_OFFDUTYSTATE] integerValue] dutyState:XDClockDutyStateOff completion:^(UIViewController *viewController){}];
}

- (void)searchAction:(id)sender
{
    XDClockQueryViewController *queryVC = [[XDClockQueryViewController alloc] init];
    [self.navigationController pushViewController:queryVC animated:YES];
}

@end
