//
//  XDEditAuditViewController.h
//  XDUI
//
//  Created by xieyajie on 13-10-24.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    XDAuditStateDisagree   = 0,
    XDAuditStateReview,
    XDAuditStateAgree,
}XDAuditState;

@interface XDEditAuditViewController : UITableViewController

- (id)initWithStyle:(UITableViewStyle)style auditState:(XDAuditState)state;

@end
