//
//  XDCreateAuditViewController.m
//  XDUI
//
//  Created by xie yajie on 13-10-22.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDCreateAuditViewController.h"

#import "XDPublicView.h"
#import "XDRemarkViewController.h"

#define KAUDITCELLROW_PERSON 0
#define KAUDITCELLROW_RANGE 1
#define KAUDITCELLROW_DESCRIBE 0
#define KAUDITCELLROW_IMAGE 0
#define KAUDITCELLROW_FINANCE 0
#define KAUDITCELLROW_MONEY 1

@interface XDCreateAuditViewController ()<XDPublicImageViewDelegate>
{
    NSMutableArray *_imageSource;
    
    UISwitch *_financeSwitch;
    BOOL _isFinanceOpen;
}

@property (strong, nonatomic) XDPublicView *personView;//选择审批人
@property (strong, nonatomic) XDPublicView *rangeView;//抄送范围
@property (strong, nonatomic) XDPublicView *describeView;//审批描述
@property (strong, nonatomic) XDPublicView *imageView;//照片
@property (strong, nonatomic) XDPublicView *financeView;//财务审批
@property (strong, nonatomic) XDPublicView *moneyView;//审批金额

@end

@implementation XDCreateAuditViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _imageSource = [NSMutableArray array];
        _isFinanceOpen = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    self.title = @"新建审核";
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"doneButtonImage.png"] style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (XDPublicView *)personView
{
    if (_personView == nil) {
        _personView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleDetail];
        _personView.titleLabel.text = @"审批人";
        _personView.detailLabel.textAlignment = NSTextAlignmentRight;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        imageView.image = [UIImage imageNamed:@""];
        _personView.rightAccessoryView = imageView;
    }
    
    return _personView;
}

- (XDPublicView *)rangeView
{
    if (_rangeView == nil) {
        _rangeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 1, self.view.frame.size.width - 20, 39) style:XDPublicViewStyleDetail];
        _rangeView.titleLabel.text = @"抄送范围";
        _rangeView.detailLabel.textAlignment = NSTextAlignmentRight;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        imageView.image = [UIImage imageNamed:@""];
        _rangeView.rightAccessoryView = imageView;
    }
    
    return _rangeView;
}

- (XDPublicView *)describeView
{
    if (_describeView == nil) {
        _describeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 100) style:XDPublicViewStyleText];
        _describeView.titleLabel.text = @"审批描述";
        _describeView.textView.placeholder = @"请输入描述...";
        
        [_describeView addTarget:self action:@selector(describeAction:)];
        [_describeView setupForRemarkWithViewJump:YES];
    }
    
    return _describeView;
}

- (XDPublicView *)imageView
{
    if (_imageView == nil) {
        _imageView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 80) style:XDPublicViewStyleImage];
        _imageView.titleLabel.text = @"照片";
        _imageView.imageDelegate = self;
        [_imageView setupWithImageSource:_imageSource canEdit:YES];
    }
    
    return _imageView;
}

- (XDPublicView *)financeView
{
    if (_financeView == nil) {
        _financeView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 40) style:XDPublicViewStyleDefault];
        _financeView.titleLabel.text = @"财务审批";
        
        _financeSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(_financeView.frame.size.width - 100, 7, 50, 37)];
        [_financeSwitch addTarget:self action:@selector(financeSwitchAction:) forControlEvents:UIControlEventValueChanged];
        [_financeView addSubview:_financeSwitch];
    }
    
    return _financeView;
}

- (XDPublicView *)moneyView
{
    if (_moneyView == nil) {
        _moneyView = [[XDPublicView alloc] initWithFrame:CGRectMake(10, 1, self.view.frame.size.width - 20, 39) style:XDPublicViewStyleDetail];
        _moneyView.titleLabel.text = @"审批金额";
        _moneyView.detailLabel.textAlignment = NSTextAlignmentRight;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        imageView.image = [UIImage imageNamed:@""];
        _moneyView.rightAccessoryView = imageView;
    }
    
    return _moneyView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
        case 3:
            if (!_isFinanceOpen) {
                return 1;
            }
            else{
                return 2;
            }
            break;
            
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NewAuditCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    NSInteger row = indexPath.row;
    switch (indexPath.section) {
        case 0:
        {
            if (row == KAUDITCELLROW_PERSON) {
                self.personView.detailLabel.text = @"陈军";
                [cell.contentView addSubview:self.personView];
            }
            else if (row == KAUDITCELLROW_RANGE) {
                self.rangeView.detailLabel.text = @"Avery，白露，陈军";
                [cell.contentView addSubview:self.rangeView];
            }
        }
            break;
        case 1:
        {
            if (row == KAUDITCELLROW_DESCRIBE) {
                [cell.contentView addSubview:self.describeView];
            }
        }
            break;
        case 2:
        {
            if (row == KAUDITCELLROW_IMAGE) {
                [cell.contentView addSubview:self.imageView];
            }
        }
            break;
        case 3:
        {
            if (row == KAUDITCELLROW_FINANCE) {
                [cell.contentView addSubview:self.financeView];
            }
            else if (row == KAUDITCELLROW_MONEY) {
                [cell.contentView addSubview:self.moneyView];
            }
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 1:
            return 100;//????
            break;
        case 2:
            return [self.imageView heightForImageState];
            break;
            
        default:
            break;
    }
    
    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if (indexPath.section == 0) {
        if (indexPath.row == KAUDITCELLROW_PERSON) {
            //????
        }
        else if (indexPath.row == KAUDITCELLROW_RANGE)
        {
            //????
        }
    }
    if (indexPath.section == 3) {
        if (indexPath.row == KAUDITCELLROW_FINANCE) {
            //????
        }
        else if (indexPath.row == KAUDITCELLROW_MONEY)
        {
            //????
        }
    }
}

#pragma mark - XDPublicImageViewDelegate

- (void)publicImageViewDidAddImage:(XDPublicView *)publicView
{
    //添加图片调用
}

- (void)publicImageView:(XDPublicView *)publicView didDeleteAtIndex:(NSInteger)index
{
    //删除图片调用
}

#pragma mark - button/item action

- (void)doneAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)describeAction:(id)sender
{
    XDRemarkViewController *remarkViewController = [[XDRemarkViewController alloc] initWithRemark:self.describeView.textView.text placeholder:@"请输入描述..."];
    [self.navigationController pushViewController:remarkViewController animated:YES];
}

- (void)financeSwitchAction:(id)sender
{
    _isFinanceOpen = _financeSwitch.isOn;
    
    [self.tableView beginUpdates];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
}

@end
