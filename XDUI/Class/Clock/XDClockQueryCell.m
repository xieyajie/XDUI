//
//  XDClockQueryCell.m
//  XDUI
//
//  Created by xie yajie on 13-10-13.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XDClockQueryCell.h"

@interface XDClockDutyView()
{
    CGFloat _imageViewOriginX;
}

@property (strong, nonatomic) UILabel *titleLabel;

@property (strong, nonatomic) UIImageView *addressImageView;
@property (strong, nonatomic) UIImageView *remarkImageView;
@property (strong, nonatomic) UIImageView *auditImageView;

@end

@implementation XDClockDutyView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.titleLabel];
        _imageViewOriginX = self.titleLabel.frame.origin.x + self.titleLabel.frame.size.width + 5;
    }
    
    return self;
}

#pragma mark - XDClockDutyView setting

- (void)setTitle:(NSString *)string
{
    if (string == nil || string.length == 0) {
        string = @"";
    }
    
    _title = string;
    self.titleLabel.text = _title;
}

- (void)setError:(BOOL)error
{
    if (error) {
        self.titleLabel.textColor = [UIColor redColor];
    }
    else{
        self.titleLabel.textColor = [UIColor greenColor];
    }
}

- (void)setAddressState:(XDClockDutyInfoState)state
{
    if (state == XDClockDutyInfoStateError || state == XDClockDutyInfoStateNormal) {
        NSString *imageName = state == XDClockDutyInfoStateError ? @"clock_addressError.png" : @"clock_addressNormal.png";
        self.addressImageView.image = [UIImage imageNamed:imageName];
        [self addSubview:self.addressImageView];
        
        if (state == XDClockDutyInfoStateError) {
            self.error = YES;
        }
    }
    else{
        [_addressImageView removeFromSuperview];
        _addressImageView = nil;
    }
}

- (void)setRemarkState:(XDClockDutyInfoState)state
{
    if (state == XDClockDutyInfoStateError || state == XDClockDutyInfoStateNormal) {
        NSString *imageName = state == XDClockDutyInfoStateError ? @"clock_remarkError.png" : @"clock_remarkNormal.png";
        self.remarkImageView.image = [UIImage imageNamed:imageName];
        [self addSubview:self.remarkImageView];
        
        if (state == XDClockDutyInfoStateError) {
            self.error = YES;
        }
    }
    else
    {
        [_remarkImageView removeFromSuperview];
        _remarkImageView = nil;
    }
}

- (void)setAuditState:(XDClockDutyInfoState)state
{
    if (state == XDClockDutyInfoStateNormal) {
        self.auditImageView.image = [UIImage imageNamed:@"clock_auditNormal.png"];
        [self addSubview:self.auditImageView];
    }
    else{
        [_auditImageView removeFromSuperview];
        _auditImageView = nil;
    }
}

#pragma mark - XDClockDutyView getting

- (UILabel *)titleLabel
{
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, self.frame.size.height)];
        _titleLabel.font = [UIFont systemFontOfSize:12.0];
        _titleLabel.textColor = [UIColor greenColor];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont systemFontOfSize:12.0];
    }
    
    return _titleLabel;
}

- (UIImageView *)addressImageView
{
    if (_addressImageView == nil) {
        _addressImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_imageViewOriginX, (self.frame.size.height - 15) / 2, 15, 15)];
        _addressImageView.contentMode = UIViewContentModeScaleAspectFit;
//        _addressImageView.backgroundColor = [UIColor redColor];
    }
    
    return _addressImageView;
}

- (UIImageView *)remarkImageView
{
    if (_remarkImageView == nil) {
        _remarkImageView = [[UIImageView alloc] init];
        _remarkImageView.contentMode = UIViewContentModeScaleAspectFit;
//        _remarkImageView.backgroundColor = [UIColor yellowColor];
    }
    
    CGFloat originX = _imageViewOriginX;
    if (_addressImageView) {
        originX += _addressImageView.frame.size.width + 12;
    }
    
    _remarkImageView.frame = CGRectMake(originX, (self.frame.size.height - 15) / 2, 15, 15);
    
    return _remarkImageView;
}

- (UIImageView *)auditImageView
{
    if (_auditImageView == nil) {
        _auditImageView = [[UIImageView alloc] init];
        _addressImageView.contentMode = UIViewContentModeScaleAspectFit;
//        _auditImageView.backgroundColor = [UIColor blueColor];
    }
    
    CGFloat originX = _imageViewOriginX;
    if (_addressImageView) {
        originX += _addressImageView.frame.size.width + 12;
    }
    if (_remarkImageView) {
        originX += _remarkImageView.frame.size.width + 12;
    }
    
    _auditImageView.frame = CGRectMake(originX, (self.frame.size.height - 15) / 2, 15, 15);
    
    return _auditImageView;
}

@end

@implementation XDClockQueryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _headerView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
        _headerView.layer.cornerRadius = 5.0;
        _headerView.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:_headerView];
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(_headerView.frame.origin.x + _headerView.frame.size.width + 10, 5, 80, 25)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.font = [UIFont systemFontOfSize:18.0];
        [self.contentView addSubview:_nameLabel];
        
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(_headerView.frame.origin.x + _headerView.frame.size.width + 10, _nameLabel.frame.origin.y + _nameLabel.frame.size.height + 5, 80, 15)];
        _dateLabel.backgroundColor = [UIColor clearColor];
        _dateLabel.textColor = [UIColor grayColor];
        _dateLabel.font = [UIFont systemFontOfSize:12.0];
        [self.contentView addSubview:_dateLabel];
        
        CGFloat originX = _nameLabel.frame.origin.x + _nameLabel.frame.size.width + 10;
        _onDutyView = [[XDClockDutyView alloc] initWithFrame:CGRectMake(originX, 10, 320 - originX - 10, 20)];
        _onDutyView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_onDutyView];
        
        _offDutyView = [[XDClockDutyView alloc] initWithFrame:CGRectMake(originX, _onDutyView.frame.origin.y + _onDutyView.frame.size.height, self.frame.size.width - originX - 10, 20)];
        _offDutyView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_offDutyView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - XDClockQueryCell getting

#pragma mark - XDClockQueryCell setting

- (void)setHeaderImageName:(NSString *)headerImageName
{
    if (headerImageName == nil || headerImageName.length == 0) {
        headerImageName = @"clock_headerImageDefult.png";
    }
    _headerView.image = [UIImage imageNamed:headerImageName];
}

- (void)setName:(NSString *)name
{
    _nameLabel.text = name;
}

- (void)setDateString:(NSString *)dateString
{
    _dateLabel.text = dateString;
}

@end
