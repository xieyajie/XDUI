//
//  XDRemarkViewController.m
//  XDUI
//
//  Created by xie yajie on 13-10-21.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDRemarkViewController.h"

#import "XDTextView.h"

#define KREMARK_MIN_HEIGHT 80
#define KREMARK_MAX_HEIGHT 120

@interface XDRemarkViewController ()<UITextViewDelegate>
{
    NSString *_remark;
    NSString *_placeholder;
    XDTextView *_textView;
}

@end

@implementation XDRemarkViewController

- (id)initWithRemark:(NSString *)text placeholder:(NSString *)placeholder
{
    self = [super init];
    if (self) {
        // Custom initialization
        _remark = text == nil ? @"" : text;
        _placeholder = placeholder == nil ? @"" : placeholder;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"编辑备注";
    self.view.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"doneButtonImage.png"] style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
    self.navigationItem.rightBarButtonItem = doneItem;
    
    _textView = [[XDTextView alloc] initWithFrame:CGRectMake(10, self.originY + 20, self.view.frame.size.width - 20, KREMARK_MIN_HEIGHT)];
    _textView.contentInset = UIEdgeInsetsZero;
    _textView.font = [UIFont systemFontOfSize:16.0];
    _textView.delegate = self;
    _textView.placeholder = _placeholder;
    _textView.text = _remark;
    
    CGFloat height = [self heightForTextViewWithString:_textView.text];
    _textView.frame = CGRectMake(10, self.originY + 20, self.view.frame.size.width - 20, height);
    [self.view addSubview:_textView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat height = textView.contentSize.height > textView.frame.size.height ? textView.contentSize.height : textView.frame.size.height;
    height = height > KREMARK_MAX_HEIGHT ? KREMARK_MAX_HEIGHT : height;
    
    if (textView.contentSize.height > textView.frame.size.height) {
        [UIView animateWithDuration:.5f animations:^{
            CGRect rect = textView.frame;
            textView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, KREMARK_MAX_HEIGHT + 16);
        }];
    }
}

#pragma mark - private

- (CGFloat)heightForTextViewWithString:(NSString *)string
{
    float fPadding = 16.0; // 8.0px x 2
    CGSize constraint = CGSizeMake(_textView.frame.size.width - fPadding, CGFLOAT_MAX);
    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:16.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat height = size.height > KREMARK_MIN_HEIGHT ? size.height : KREMARK_MIN_HEIGHT;
    height = height > KREMARK_MAX_HEIGHT ? KREMARK_MAX_HEIGHT : height;
    
    return height + fPadding;
}

#pragma mark - item action

- (void)doneAction:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"remarkEditFinish" object:_textView.text];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
