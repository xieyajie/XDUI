//
//  XDActivityView.h
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XDAuditModel.h"//审核
#import "XDCommentModel.h"//评论
#import "XDActivityModel.h"//动态

@protocol XDActivityViewDelegate;

@interface XDActivityView : UIView

@property (nonatomic, weak) id<XDActivityViewDelegate> delegate;

@property (nonatomic, strong) XDActivityModel *activity;  //动态数据源
@property (nonatomic, strong) NSDictionary *otherInfo;  //其他信息

- (id)initWithFrame:(CGRect)frame activity:(XDActivityModel *)activity;

- (CGFloat)activityViewHeight;
- (void)setupActivityView;

@end

@protocol XDActivityViewDelegate <NSObject>

@required
- (void)contactActivityViewCommentButtonClicked:(XDActivityView *)activityView; //点击评论按钮
- (void)contactActivityViewAuditButtonClicked:(XDActivityView *)activityView; //点击地址按钮

@end
