//
//  XDClockInViewController.h
//  XDUI
//
//  Created by xie yajie on 13-10-12.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    XDClockDutyStateOn  = 0,
    XDClockDutyStateOff,
}XDClockDutyState;

@interface XDClockInViewController : UITableViewController

- (void)configurationDataWithDutyState:(XDClockDutyState)state;

@end
