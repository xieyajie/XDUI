//
//  XDNoticeDetailViewController.m
//  XDUI
//
//  Created by xieyajie on 13-11-6.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XDNoticeDetailViewController.h"

#import "NSDate+Category.h"

#define KCELLROW_TITLE 0
#define KCELLROW_ACCOUNT 1
#define KCELLROW_CONTENT 2

@interface XDNoticeDetailViewController ()
{
    NSMutableDictionary *_dataSource;
}

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *contentLabel;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UIImageView *headImageView;
@property (strong, nonatomic) UIView *accountLine;

@property (nonatomic) CGFloat contentHeight;

@end

@implementation XDNoticeDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _dataSource = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    self.title = @"公告正文";
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.tableView.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self configurationData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (UILabel *)titleLabel
{
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.tableView.frame.size.width - 20, 20)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _titleLabel;
}

- (UILabel *)contentLabel
{
    if (_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, self.tableView.frame.size.width - 20, self.contentHeight)];
        _contentLabel.backgroundColor = [UIColor clearColor];
        _contentLabel.numberOfLines = 0;
        _contentLabel.font = [UIFont systemFontOfSize:14.0];
    }
    
    return _contentLabel;
}

- (UIImageView *)headImageView
{
    if (_headImageView == nil) {
        _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        _headImageView.layer.cornerRadius = 5.0;
        _headImageView.layer.borderWidth = 1;
        _headImageView.layer.borderColor = [[UIColor colorWithWhite:0.5 alpha:0.8] CGColor];
    }
    
    return _headImageView;
}

- (UILabel *)nameLabel
{
    if (_nameLabel == nil) {
        CGFloat originX = self.headImageView.frame.origin.x + self.headImageView.frame.size.width + 10;
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(originX, 10, self.tableView.frame.size.width - originX - 110, 20)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.font = [UIFont systemFontOfSize:14.0];
    }
    
    return _nameLabel;
}

- (UILabel *)dateLabel
{
    if (_dateLabel == nil) {
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.tableView.frame.size.width - 10 - 100, 10, 100, 20)];
        _dateLabel.textAlignment = NSTextAlignmentRight;
        _dateLabel.backgroundColor = [UIColor clearColor];
        _dateLabel.font = [UIFont systemFontOfSize:14.0];
    }
    
    return _dateLabel;
}

- (UIView *)accountLine
{
    if (_accountLine == nil) {
        _accountLine = [[UIView alloc] initWithFrame:CGRectMake(10, self.headImageView.frame.origin.y + self.headImageView.frame.size.height + 9, self.tableView.frame.size.width - 20, 1)];
        _accountLine.backgroundColor = [UIColor grayColor];
    }
    
    return _accountLine;
}

- (CGFloat)contentHeight
{
    NSString *content = [_dataSource objectForKey:KNOTICE_CONTENT];
    CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(self.tableView.frame.size.width - 20, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    return size.height;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NoticeDetailCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    switch (indexPath.row) {
        case KCELLROW_TITLE:
            [self configurationTitleCell:cell withString:[_dataSource objectForKey:KNOTICE_TITLE]];
            break;
        case KCELLROW_ACCOUNT:
            [self configurationAccountCell:cell withImagePath:[_dataSource objectForKey:KNOTICE_HEADPATH] withName:[_dataSource objectForKey:KNOTICE_NAME] withDate:[_dataSource objectForKey:KNOTICE_DATE]];
            break;
        case KCELLROW_CONTENT:
            [self configurationContentCell:cell withString:[_dataSource objectForKey:KNOTICE_CONTENT]];
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case KCELLROW_TITLE:
            return 20 + 20;
            break;
        case KCELLROW_ACCOUNT:
            return 50 + 20;
            break;
        case KCELLROW_CONTENT:
            return self.contentHeight + 30;
            break;
            
        default:
            return 0;
            break;
    }
}

#pragma mark - cell

- (void)configurationTitleCell:(UITableViewCell *)aCell withString:(NSString *)string
{
    self.titleLabel.text = string;
    [aCell.contentView addSubview:self.titleLabel];
}

- (void)configurationAccountCell:(UITableViewCell *)aCell withImagePath:(NSString *)imgPath withName:(NSString *)name withDate:(NSDate *)date
{
    self.headImageView.image = [UIImage imageNamed:imgPath];
    self.nameLabel.text = [NSString stringWithFormat:@"发布者：%@", name];
    self.dateLabel.text = [date formattedDateDescription];
    
    [aCell.contentView addSubview:self.headImageView];
    [aCell.contentView addSubview:self.nameLabel];
    [aCell.contentView addSubview:self.dateLabel];
    [aCell.contentView addSubview:self.accountLine];
}

- (void)configurationContentCell:(UITableViewCell *)aCell withString:(NSString *)string
{
    self.contentLabel.text = string;
    [aCell.contentView addSubview:self.contentLabel];
}

#pragma mark - data

- (void)configurationData
{
    //从服务器获取数据
    //????
    
    //测试数据
    _dataSource = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"Jervis Liu", KNOTICE_NAME, @"", KNOTICE_HEADPATH, @"五一放假通知", KNOTICE_TITLE, @"公司全体员工：劳动节将来临，根据公司规定5.1劳动节放假一天，祝大家节日快乐！", KNOTICE_CONTENT, [NSDate date], KNOTICE_DATE, nil];
    
    //    [self.tableView reloadData];
}

@end
