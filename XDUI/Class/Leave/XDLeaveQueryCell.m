//
//  XDLeaveQueryCell.m
//  XDUI
//
//  Created by xieyajie on 13-11-7.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XDLeaveQueryCell.h"

@implementation XDLeaveQueryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _headerView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
        _headerView.layer.cornerRadius = 5.0;
        _headerView.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:_headerView];
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(_headerView.frame.origin.x + _headerView.frame.size.width + 10, 10, 80, 20)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.font = [UIFont systemFontOfSize:18.0];
        [self.contentView addSubview:_nameLabel];
        
        CGFloat originX = _nameLabel.frame.origin.x + _nameLabel.frame.size.width + 10;
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(originX, 10, self.frame.size.width - 10 - originX, 20)];
        _dateLabel.backgroundColor = [UIColor clearColor];
        _dateLabel.textColor = [UIColor grayColor];
        _dateLabel.textAlignment = NSTextAlignmentRight;
        _dateLabel.font = [UIFont systemFontOfSize:12.0];
        [self.contentView addSubview:_dateLabel];
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(_headerView.frame.origin.x + _headerView.frame.size.width + 10, _nameLabel.frame.origin.y + _nameLabel.frame.size.height + 5, self.frame.size.width - 10 - (_headerView.frame.origin.x + _headerView.frame.size.width + 10), 20)];
        _contentLabel.backgroundColor = [UIColor clearColor];
        _contentLabel.font = [UIFont systemFontOfSize:14.0];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:_contentLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - XDClockQueryCell setting

- (void)setHeaderImageName:(NSString *)headerImageName
{
    if (headerImageName == nil || headerImageName.length == 0) {
        headerImageName = @"clock_headerImageDefult.png";
    }
    _headerView.image = [UIImage imageNamed:headerImageName];
}

- (void)setName:(NSString *)name
{
    _nameLabel.text = name;
}

- (void)setDateString:(NSString *)dateString
{
    _dateLabel.text = dateString;
}

- (void)setContent:(NSString *)string
{
    _contentLabel.text = string;
}

@end
