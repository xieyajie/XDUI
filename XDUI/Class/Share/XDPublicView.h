//
//  XDPublicView.h
//  XDUI
//
//  Created by xieyajie on 13-10-24.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XDTextView.h"

typedef enum{
    XDPublicViewStyleDefault   = 0,
    XDPublicViewStyleDetail,
    XDPublicViewStyleImage,
    XDPublicViewStyleText,
}XDPublicViewStyle;

@protocol XDPublicImageViewDelegate;

@interface XDPublicView : UIView

@property (unsafe_unretained, nonatomic) id<XDPublicImageViewDelegate> imageDelegate;

@property (strong, nonatomic) UIView *leftAccessoryView;
@property (strong, nonatomic) UIView *rightAccessoryView;

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *detailLabel;
@property (strong, nonatomic) XDTextView *textView;
@property (strong, nonatomic) UIView *contentView;

- (id)initWithStyle:(XDPublicViewStyle)style;
- (id)initWithFrame:(CGRect)frame style:(XDPublicViewStyle)style;

- (void)addTarget:(id)target action:(SEL)action;
- (void)clearContentView;

//detail style
- (CGFloat)heightWithDetailText:(NSString *)string;

//text样式
- (CGFloat)heightForTextViewWithString:(NSString *)string;
- (void)setupForRemarkWithViewJump:(BOOL)jump;

//图片样式
- (CGFloat)heightForImageState;
- (void)setupWithImageSource:(NSArray *)array canEdit:(BOOL)edit;
- (void)setupWithImageSource:(NSArray *)array;
- (void)addImageToView:(UIImage *)image;

@end


@protocol XDPublicImageViewDelegate <NSObject>

@required
- (void)publicImageViewDidAddImage:(XDPublicView *)publicView;
- (void)publicImageView:(XDPublicView *)publicView didDeleteAtIndex:(NSInteger)index;

@end
