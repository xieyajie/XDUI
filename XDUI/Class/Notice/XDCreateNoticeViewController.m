//
//  XDCreateNoticeViewController.m
//  XDUI
//
//  Created by xieyajie on 13-11-6.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDCreateNoticeViewController.h"

#import "XDTextView.h"

#define KCELLROW_TITLE 0
#define KCELLROW_CONTENT 1

#define KCELLCONTENT_DEFAULT_HEIGHT 100

@interface XDCreateNoticeViewController ()
{
    UITextField *_titleField;
}

@property (strong, nonatomic) UIView *titleView;
@property (strong, nonatomic) XDTextView *contentView;

@end

@implementation XDCreateNoticeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"发布公告";
    
    UIBarButtonItem *createItem = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStyleBordered target:self action:@selector(createAction:)];
    self.navigationItem.rightBarButtonItem = createItem;
    
    self.view.backgroundColor = [UIColor colorWithRed:242 / 255.0 green:240 / 255.0 blue:235 / 255.0 alpha:1.0];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getting

- (UIView *)titleView
{
    if (_titleView == nil) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 40)];
        _titleView.backgroundColor = [UIColor whiteColor];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 30, 20)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont systemFontOfSize:14.0];
        titleLabel.textColor = [UIColor grayColor];
        titleLabel.text = @"标题";
        [_titleView addSubview:titleLabel];
        
        _titleField = [[UITextField alloc] initWithFrame:CGRectMake(50, 10, _titleView.frame.size.width - 60, 20)];
        _titleField.font = [UIFont systemFontOfSize:14.0];
        _titleField.borderStyle = UITextBorderStyleNone;
        _titleField.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
        _titleField.returnKeyType = UIReturnKeyDone;
        [_titleView addSubview:_titleField];
    }
    
    return _titleView;
}

- (XDTextView *)contentView
{
    if (_contentView == nil) {
        _contentView = [[XDTextView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, KCELLCONTENT_DEFAULT_HEIGHT)];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.font = [UIFont systemFontOfSize:14.0];
        _contentView.placeholder = @"公告详情";
    }
    
    return _contentView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NoticeNewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    else{
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    switch (indexPath.row) {
        case KCELLROW_TITLE:
            [self configurationTitleCell:cell];
            break;
        case KCELLROW_CONTENT:
            [self configurationContentCell:cell];
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case KCELLROW_TITLE:
            return 40 + 20;
            break;
        case KCELLROW_CONTENT:
            return self.contentView.frame.size.height + 10;
            break;
            
        default:
            return 0;
            break;
    }
}

#pragma mark - cell

- (void)configurationTitleCell:(UITableViewCell *)aCell
{
    [aCell.contentView addSubview:self.titleView];
}

- (void)configurationContentCell:(UITableViewCell *)aCell
{
    [aCell.contentView addSubview:self.contentView];
}

#pragma mark - button/item action

- (void)createAction:(id)sender
{
    //????
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
