//
//  LKScrollView.h
//  Travel
//
//  Created by tixa tixa on 13-4-23.
//  Copyright (c) 2013年 tixa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LKImageView.h"

@class LKImageManager;

@interface LKScrollView : UIView<UIScrollViewDelegate>

+ (id)scrollViewWithImageView:(LKImageView *)imageView;

@property (nonatomic, readonly, strong) LKImageView *imageView;

- (void)enterFullScreen:(BOOL)animated;
- (void)quitFullScreen;

- (BOOL)isLoading;
- (void)loadImage;
- (void)cancelLoadImage;
- (void)recoverDoubleView;

@property (nonatomic , weak) LKImageManager *manager;

@end
