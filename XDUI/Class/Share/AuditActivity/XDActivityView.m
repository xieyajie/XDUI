//
//  XDActivityView.m
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "XDActivityView.h"

#import "RCLabel.h"
#import "XDAppendixView.h"

#import "NSString+Category.h"
#import "NSDate+Category.h"

#define FOLD_TEXT_LENGTH 200
#define SHOW_LIST_MAXCOUNT 3

@interface XDActivityView () <RTLabelDelegate>

@property (nonatomic, strong) UIButton *auditButton;

@end

@implementation XDActivityView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame activity:(XDActivityModel *)activity
{
    self = [self initWithFrame:frame];
    if (self) {
        _activity = activity;
    }
    return self;
}

#pragma mark - getting

- (UIButton *)auditButton
{
    if (_auditButton == nil) {
        _auditButton = [[UIButton alloc] init];
        [_auditButton setTitle:@"处理" forState:UIControlStateNormal];
        [_auditButton addTarget:self action:@selector(clickAuditButton:) forControlEvents:UIControlEventTouchUpInside];
        [_auditButton setImage:[UIImage imageNamed:@"audit_icon_audit.png"] forState:UIControlStateNormal];
        _auditButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 10.0, 0.0, 30.0);
        _auditButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _auditButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_auditButton setTitleColor:[UIColor colorWithRed:113 / 255.0 green:132 / 255.0 blue:173 / 255.0 alpha:1.0] forState:UIControlStateNormal];
    }
    
    return _auditButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - RTLabel Delegate

- (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSString*)url
{
    
}


#pragma mark - private

#pragma mark - tool

- (NSString *)cutString:(NSString *)string toLength:(NSInteger)length
{
    if (string == nil) {
        return @"";
    }
    
    if (string.length <= length) {
        return string;
    }
    else{
        return [NSString stringWithFormat:@"%@...", [string substringToIndex:length]];
    }
}

#pragma mark - headImage
/*头像视图*/
- (UIImageView *)createHeadImageViewWithFrame:(CGRect)frame
                                       headPath:(NSString *)headPath
                                         gender:(XDAccountGender)gender
                                      accountID:(NSUInteger)accountID
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.layer.borderColor = [[UIColor colorWithWhite:0.6 alpha:0.4] CGColor];
    imageView.layer.borderWidth = 1.0;
    //测试数据
    //????
    imageView.image = [UIImage imageNamed:headPath];
    
    return imageView;
}

/*数据源头像*/
- (UIImageView *)createHeadImageViewWithFrame:(CGRect)frame
{
    if (_activity) {
        return [self createHeadImageViewWithFrame:frame headPath:_activity.headImagePath gender:_activity.gender accountID:_activity.accountId.integerValue];
    }
    
    return nil;
}

#pragma mark - name, title, content

/*通用标签高度*/
- (CGFloat)labelHeightWithFrame:(CGRect)frame text:(NSString *)text font:(UIFont *)font multiLine:(BOOL)multiLine
{
    if (multiLine) {
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, 9999.0) lineBreakMode:NSLineBreakByCharWrapping];
        return size.height;
    }
    else {
        return frame.size.height;
    }
    return 0.0;
}

/*通用标签*/
- (UILabel *)createLabelWithFrame:(CGRect)frame text:(NSString *)text font:(UIFont *)font textColor:(UIColor *)textColor multiLine:(BOOL)multiLine
{
    UILabel *textLabel = [[UILabel alloc] initWithFrame:frame];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.highlightedTextColor = [UIColor whiteColor];
    textLabel.textColor = textColor;
    textLabel.font = font;
    textLabel.text = text;
    if (multiLine) {
        textLabel.numberOfLines = 0;
        textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width, 9999.0) lineBreakMode:NSLineBreakByCharWrapping];
        frame.size.height = size.height;
        textLabel.frame = frame;
    }
    return textLabel;
}

/*姓名标签*/
- (UILabel *)createNameLabelWithFrame:(CGRect)frame text:(NSString *)text
{
    return [self createLabelWithFrame:frame
                                 text:text
                                 font:[UIFont boldSystemFontOfSize:17.0]
                            textColor:[UIColor blackColor]
                            multiLine:NO];
}

/*数据源姓名标签*/
- (UILabel *)createNameLabelWithFrame:(CGRect)frame
{
    if (_activity)
    {
        return [self createNameLabelWithFrame:frame text:_activity.name];
    }
    
    return nil;
}

#pragma mark - Appendix View

/*附件视图高度*/
- (CGFloat)appendixViewHeightWithFrame:(CGRect)frame appendixs:(NSArray *)appendixArray appendixType:(XDMediaType)appendixType thumbnailMode:(BOOL)thumbnailMode
{
    switch (appendixType) {
        case XDMediaTypeImage: {
            CGFloat l = (frame.size.width - 10.0) / 3.0;
            if (appendixArray.count > 6) {
                return l * 3 + 5.0 * 2;
            } else if (appendixArray.count > 3) {
                return l * 2 + 5.0;
            } else if (appendixArray.count > 1) {
                return l;
            } else {
                return thumbnailMode ? THUMB_IMAGE_SIZE.height : BIG_IMAGE_SIZE.height;
            }
            break;
        }
            
        case XDMediaTypeGIF: case XDMediaTypeVideo:
            return thumbnailMode ? THUMB_IMAGE_SIZE.height : BIG_IMAGE_SIZE.height;
            break;
            
        case XDMediaTypeAudio:
            return AUDIO_IMAGE_SIZE.height;
            break;
            
        case XDMediaTypeMood:
            return MOOD_IMAGE_SIZE.height;
            break;
            
        default:
            return 0.0;
            break;
    }
    
    return 0;
}

- (UIView *)createImageAppendixViewWithFrame:(CGRect)frame
                                    appendix:(NSArray *)appendixArray
                               thumbnailMode:(BOOL)thumbnailMode
{
    CGRect mFrame = frame;
    mFrame.size.height = [self appendixViewHeightWithFrame:frame appendixs:appendixArray appendixType:XDMediaTypeImage thumbnailMode:thumbnailMode];
    XDAppendixView *imagesView = [[XDAppendixView alloc] initWithFrame:mFrame
                                                              medias:appendixArray
                                                      layoutStyle:XDMediaViewLayoutStyleScaleToFill];
//    imagesView.backgroundColor = [UIColor yellowColor];
    return imagesView;
}

- (UIView *)createVideoAppendixViewWithFrame:(CGRect)frame
                                    appendix:(NSArray *)appendixArray
                               thumbnailMode:(BOOL)thumbnailMode
{
    return nil;
}

- (UIView *)createAudioAppendixViewWithFrame:(CGRect)frame
                                    appendix:(NSArray *)appendixArray
                               thumbnailMode:(BOOL)thumbnailMode
{
    return nil;
}

- (UIView *)createAppendixViewWithFrame:(CGRect)frame
                               appendix:(NSArray *)appendixArray
                           appendixType:(XDMediaType)appendixType
                          thumbnailMode:(BOOL)thumbnailMode
{
    switch (appendixType) {
        case XDMediaTypeImage:
            return [self createImageAppendixViewWithFrame:frame appendix:appendixArray thumbnailMode:thumbnailMode];
            break;
        case XDMediaTypeVideo:
            return [self createVideoAppendixViewWithFrame:frame appendix:appendixArray thumbnailMode:thumbnailMode];
            break;
        case XDMediaTypeAudio:
            return [self createAudioAppendixViewWithFrame:frame appendix:appendixArray thumbnailMode:thumbnailMode];
            break;
            
        default:
            return nil;
            break;
    }
}

/*附件视图*/
- (UIView *)createAppendixViewWithFrame:(CGRect)frame
                           appendix:(NSDictionary *)appendixDictionary
                          thumbnailMode:(BOOL)thumbnailMode
{
    UIView *appendixView = [[UIView alloc] initWithFrame:frame];
//    appendixView.backgroundColor = [UIColor redColor];
    
    CGFloat y = 0;
    UIView *tmpView = nil;
    for (NSString *key in appendixDictionary) {
        y += 5;
        
        NSArray *array = [appendixDictionary objectForKey:key];
        tmpView = [self createAppendixViewWithFrame:CGRectMake(0, y, frame.size.width, 0.0) appendix:array appendixType:[key integerValue] thumbnailMode:thumbnailMode];
        if (tmpView) {
            [appendixView addSubview:tmpView];
            y = tmpView.frame.origin.y + tmpView.frame.size.height;
        }
    }
    
    appendixView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, y + 5);
    return appendixView;
}

#pragma mark - Rich View

/*富文本视图高度*/
- (CGFloat)richViewHeightWithFrame:(CGRect)frame text:(NSString *)text
{
    if (text && text.length > 0) {
        RCLabel *richLabel = [[RCLabel alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
        richLabel.lineBreakMode = RTTextLineBreakModeCharWrapping;
        richLabel.componentsAndPlainText = [RCLabel extractTextStyle:text];
        
        return richLabel.optimumSize.height + 5.0;
    }
    else{
        return 0.0;
    }
}

/*富文本视图*/
- (UIView *)createActivityRichViewWithFrame:(CGRect)frame
                                   title:(NSString *)title
                                 finance:(NSString *)finance
                                 content:(NSString *)content
                           thumbnailMode:(BOOL)thumbnailMode
{
    RCLabel *richLabel = [[RCLabel alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
    richLabel.delegate = self;
    richLabel.lineBreakMode = RTTextLineBreakModeCharWrapping;
    
    NSMutableString *htmlString = [NSMutableString string];
    if (title && title.length > 0) {
        [htmlString appendFormat:@"<font size=14 color=gray>%@</font>", title];
    }
    if (finance && finance.length > 0) {
        [htmlString appendFormat:@"<font size=14> %@</font>", finance];
    }
    if (content && content.length > 0) {
        NSMutableString *str = [NSMutableString string];
        if (finance && finance.length > 0) {
            [str appendString:@","];
        }
        {
            [str appendString:@" "];
        }
        
        if (thumbnailMode)
        {
            [str appendString:[self cutString:content toLength:FOLD_TEXT_LENGTH]];
        }
        else{
            [str appendString:content];
        }
        
        
        [htmlString appendFormat:@"<font size=14>%@</font>", str];
    }
    
    if (htmlString.length > 0) {
        richLabel.componentsAndPlainText = [RCLabel extractTextStyle:htmlString];
        
        CGSize size = richLabel.optimumSize;
        CGRect rFrame = CGRectZero;
        rFrame.origin.x = frame.origin.x;//偏移量微调
        rFrame.origin.y = frame.origin.y;
        rFrame.size.width = frame.size.width;
        rFrame.size.height = size.height + 5.0;
        richLabel.frame = rFrame;
        
        return richLabel;
    }
    
    return nil;
}

- (UIView *)createAuditRichViewWithFrame:(CGRect)frame
                               auditList:(NSArray *)auditList
                           thumbnailMode:(BOOL)thumbnailMode
{
    UIView *view = [[UIView alloc] initWithFrame:frame];
    CGFloat y = 10.0;
    CGFloat auditW = frame.size.width - 30;
    
    //流程开始图片
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(5.0, y, 15, 13)];
    iconView.image = [UIImage imageNamed:@"audit_icon_audit.png"];
    [view addSubview:iconView];
    
    UILabel *iconLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, y, frame.size.width - 35, 13)];
    iconLabel.backgroundColor = [UIColor clearColor];
    iconLabel.textColor = [UIColor grayColor];
    iconLabel.font = [UIFont systemFontOfSize:10.0];
    iconLabel.text = @"以下为审批人意见：";
    [view addSubview:iconLabel];
    
    y += iconView.frame.size.height;
    
    //列表
    NSInteger auditCount = auditList.count;
    if (thumbnailMode) {
        auditCount = auditList.count > SHOW_LIST_MAXCOUNT ? SHOW_LIST_MAXCOUNT : auditList.count;
    }
    for (NSInteger i = 0; i < auditCount; i++) {
        //内容显示
        XDAuditModel *auditModel = [auditList objectAtIndex:i];
        
        RCLabel *richLabel = [[RCLabel alloc] initWithFrame:CGRectMake(25.0, y, auditW, 0.0)];
        richLabel.delegate = self;
        richLabel.lineBreakMode = RTTextLineBreakModeCharWrapping;
        
        NSMutableString *htmlString = [NSMutableString string];
        if (auditModel.name.length > 0) {
            NSString *content = auditModel.content;
            if (thumbnailMode)
            {
                content = [self cutString:auditModel.content toLength:FOLD_TEXT_LENGTH];
            }
            [htmlString appendFormat:@"<font size=10>%@: %@</font>", auditModel.name, content];
        }
        if(auditModel.createDate != nil)
        {
            [htmlString appendFormat:@"\n<font size=10 color=gray>%@</font>", auditModel.createDate.timeIntervalDescription];
        }
        if (auditModel.stateDescription.length > 0) {
            if (auditModel.state == -1) {
                [htmlString appendFormat:@"<font size=10 color=red>    %@</font>", auditModel.stateDescription];
            }
            else if (auditModel.state == 0){
                [htmlString appendFormat:@"<font size=10 color=green>    %@</font>", auditModel.stateDescription];
            }
        }
        
        if (htmlString.length > 0) {
            richLabel.componentsAndPlainText = [RCLabel extractTextStyle:htmlString];
            
            CGSize size = richLabel.optimumSize;
            CGRect rFrame = richLabel.frame;
            rFrame.size.height = size.height + 5.0;
            richLabel.frame = rFrame;
            [view addSubview:richLabel];
            
            //处理圆点图片
            UIImageView *flowView = [[UIImageView alloc] initWithFrame:CGRectMake(7.0, y, 11, rFrame.size.height)];
            flowView.contentMode = UIViewContentModeScaleAspectFit;
            flowView.image = [UIImage imageNamed:@"audit_point.png"];
            [view addSubview:flowView];
            
            y += richLabel.frame.size.height;
        }
    }

    //坐标y调整
    y += 10;
    
    //流程箭头
    CGFloat originY = iconView.frame.origin.y + iconView.frame.size.height;
    UIImageView *flowEndView = [[UIImageView alloc] initWithFrame:CGRectMake(5.0, originY, 15, y - originY - 3)];
    flowEndView.image = [[UIImage imageNamed:@"audit_flow_audit_end.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:2];
    [view addSubview:flowEndView];
    [view sendSubviewToBack:flowEndView];
    
    view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height + y);
    
    return view;
}

- (UIView *)createCommentRichViewWithFrame:(CGRect)frame
                                   commentList:(NSArray *)commentList
                                 thumbnailMode:(BOOL)thumbnailMode
{
    UIView *view = [[UIView alloc] initWithFrame:frame];
    CGFloat y = 10.0;
    
    //图标
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(5.0, y, 15, 15)];
    iconView.image = [UIImage imageNamed:@"audit_icon_comment.png"];
    [view addSubview:iconView];
    
    CGFloat commentW = frame.size.width - 30;
    
    //列表
    NSInteger commentCount = commentList.count;
    if (thumbnailMode) {
        commentCount = commentList.count > SHOW_LIST_MAXCOUNT ? SHOW_LIST_MAXCOUNT : commentList.count;
    }
    for (NSInteger i = 0; i < commentCount; i++) {
        XDCommentModel *commentModel = [commentList objectAtIndex:i];
        
        RCLabel *richLabel = [[RCLabel alloc] initWithFrame:CGRectMake(25.0, y, commentW, 0.0)];
        richLabel.delegate = self;
        richLabel.lineBreakMode = RTTextLineBreakModeCharWrapping;
        
        NSMutableString *htmlString = [NSMutableString string];
        if (commentModel.name.length > 0) {
            [htmlString appendFormat:@"<font size=10 color=#8f9dbb>%@:</font>", commentModel.name];
        }
        if (commentModel.content.length > 0) {
            NSString *content = commentModel.content;
            if (thumbnailMode)
            {
                content = [self cutString:commentModel.content toLength:FOLD_TEXT_LENGTH];
            }
            [htmlString appendFormat:@"<font size=10> %@</font>", content];
        }
        
        if (htmlString.length > 0) {
            richLabel.componentsAndPlainText = [RCLabel extractTextStyle:htmlString];
            
            CGSize size = richLabel.optimumSize;
            CGRect rFrame = richLabel.frame;
            rFrame.size.height = size.height;
            richLabel.frame = rFrame;
            [view addSubview:richLabel];
            
            y += richLabel.frame.size.height + 5.0;
        }
    }
    
    view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height + y);

    return view;
}

#pragma mark - view setup

- (CGFloat)activityViewHeightWithShowHead:(BOOL)showHead
                                 showName:(BOOL)showName
                                 showInfo:(BOOL)showInfo
                             showAppendix:(BOOL)showAppendix
                              showAddress:(BOOL)showAddress
                           showTimeSource:(BOOL)showTimeSource
                               showAudits:(BOOL)showAudits
                             showComments:(BOOL)showComments
                            thumbnailMode:(BOOL)thumbnailMode
{
    CGFloat cX = 10.0;
    CGFloat cY = 10.0;
    CGFloat cW = self.frame.size.width - 20.0;
    
    //发送者头像
    if (showHead) {
        cX = 58.0;
        cW = self.frame.size.width - cX - 12.0;
    }
    
    //发送者姓名
//    NSLog(@"Height: name-b--%f", cY);
    if (showName) {
        cY = cY + 20.0;
    }
    
    //正文
//    NSLog(@"Height: info-b--%f", cY);
    if (showInfo) {
        NSMutableString *htmlString = [NSMutableString string];
        if (_activity.title && _activity.title.length > 0) {
            [htmlString appendFormat:@"<font size=14 color=gray>%@</font>", _activity.title];
        }
        if (_activity.finance && _activity.finance.length > 0) {
            [htmlString appendFormat:@"<font size=14> %@</font>", _activity.finance];
        }
        if (_activity.content && _activity.content.length > 0) {
            NSMutableString *str = [NSMutableString string];
            if (_activity.finance && _activity.finance.length > 0) {
                [str appendString:@","];
            }
            {
                [str appendString:@" "];
            }
            
            if (thumbnailMode)
            {
                [str appendString:[self cutString:_activity.content toLength:FOLD_TEXT_LENGTH]];
            }
            else{
                [str appendString:_activity.content];
            }
            
            
            [htmlString appendFormat:@"<font size=14>%@</font>", str];
        }
        
        CGFloat contentHeight = [self richViewHeightWithFrame:CGRectMake(cX, cY + 5.0, cW, 0.0)
                                                         text:htmlString];
        cY = cY + contentHeight + 5;
    }
    
    //附件视图
//    NSLog(@"Height: appendix-b--%f", cY);
    if (showAppendix) {
        CGFloat appendixHeight = 5;
        for (NSString *key in _activity.appendixDictionary) {
            appendixHeight += 5;
            
            NSArray *array = [_activity.appendixDictionary objectForKey:key];
            appendixHeight += [self appendixViewHeightWithFrame:CGRectMake(cX, cY + appendixHeight, cW, 0.0) appendixs:array appendixType:[key integerValue]  thumbnailMode:thumbnailMode];
            
        }
        
        cY += appendixHeight + 5;
    }
    
    //位置
//    NSLog(@"Height: address-b--%f", cY);
    if (showAddress) {
        cY += 5;
        if (thumbnailMode) {
            NSString *address = _activity.address.length ? _activity.address : @"定位失败";
            UIFont *font = [UIFont systemFontOfSize:11.0];
            CGSize size = [address sizeWithFont:font constrainedToSize:CGSizeMake(cW, 40.0)];
            cY = cY + size.height;
        } else {
            cY = cY + 105.0;
        }
    }
    
    //时间、来源
//    NSLog(@"Height: date-b--%f", cY);
    if (showTimeSource) {
        cY = cY + 37.0;
    }
    
    //处理列表
//    NSLog(@"Height: audit-b--%f", cY);
    if (showAudits && _activity.isHaveAudit) {
        CGFloat y = cY + 23.0;
        for (NSInteger i = 0; i < _activity.auditList.count; i++) {
            XDAuditModel *audit = [_activity.auditList objectAtIndex:i];
            NSMutableString *htmlString = [NSMutableString string];
            if (audit.name.length > 0)
            {
                NSString *content = audit.content;
                if (thumbnailMode)
                {
                    content = [self cutString:audit.content toLength:FOLD_TEXT_LENGTH];
                }
                [htmlString appendFormat:@"<font size=10>%@: %@</font>", audit.name, content];
            }
            if(audit.createDate != nil)
            {
                [htmlString appendFormat:@"\n<font size=10 color=gray>%@</font>", audit.createDate.timeIntervalDescription];
            }
            if (audit.stateDescription.length > 0) {
                if (audit.state == -1) {
                    [htmlString appendFormat:@"<font size=10 color=red>    %@</font>", audit.stateDescription];
                }
                else if (audit.state == 0){
                    [htmlString appendFormat:@"<font size=10 color=green>    %@</font>", audit.stateDescription];
                }
            }
            CGFloat commentHeight = [self richViewHeightWithFrame:CGRectMake(cX + 25.0, y, cW - 30.0, 0.0)
                                                             text:htmlString];
            y += commentHeight;
        }
        
        cY = y + 10.0;
    }
    
    //评论列表
//    NSLog(@"Height: comment-b--%f", cY);
    if (showComments && _activity.isHaveComment) {
        CGFloat y = cY + 15.0;
        for (NSInteger i = 0; i < _activity.commentList.count; i++) {
            XDCommentModel *comment = [_activity.commentList objectAtIndex:i];
            NSMutableString *htmlString = [NSMutableString string];
            if (comment.name.length > 0) {
                [htmlString appendFormat:@"<font size=10 color=#8f9dbb>%@:</font>", comment.name];
            }
            if (comment.content.length > 0) {
                NSString *content = comment.content;
                if (thumbnailMode)
                {
                    content = [self cutString:comment.content toLength:FOLD_TEXT_LENGTH];
                }
                [htmlString appendFormat:@"<font size=10> %@</font>", content];
            }
            
            CGFloat commentHeight = [self richViewHeightWithFrame:CGRectMake(cX + 25.0, y, cW - 30.0, 0.0)
                                                             text:htmlString];
            y += commentHeight;
        }

        cY = y;
    }
    
//    NSLog(@"Height: end--%f", cY + 10);
    //修正高度
    return cY + 10.0;
}

/*动态布局 7以下*/
- (void)setupActivityViewWithShowHead:(BOOL)showHead
                             showName:(BOOL)showName
                             showInfo:(BOOL)showInfo
                         showAppendix:(BOOL)showAppendix
                          showAddress:(BOOL)showAddress
                        addressStyle:(XDAddressStyle)addressStyle
                       showTimeSource:(BOOL)showTimeSource
                      showAuditButton:(BOOL)showAuditButton
                       showAuditCount:(BOOL)showAuditCount
                           showAudits:(BOOL)showAudits
                    showCommentButton:(BOOL)showCommentButton
                     showCommentCount:(BOOL)showCommentCount
                         showComments:(BOOL)showComments
                        thumbnailMode:(BOOL)thumbnailMode
{
    CGFloat cX = 10.0;
    CGFloat cY = 10.0;
    CGFloat cW = self.frame.size.width - 20.0;
    
    //发送者头像
    if (showHead) {
        UIImageView *headImageView = [self createHeadImageViewWithFrame:CGRectMake(cX, cY, 40.0, 40.0)];
        headImageView.layer.cornerRadius = 5.0;
        [self addSubview:headImageView];
        
        cX = headImageView.frame.origin.x + headImageView.frame.size.width + 5.0;
        cW = self.frame.size.width - cX - 10;
    }
    
    //发送者姓名
//    NSLog(@"view: name-b--%f", cY);
    if (showName) {
        UILabel *nameLabel = [self createNameLabelWithFrame:CGRectMake(cX + 5, cY, 100.0, 20.0)];
        [self addSubview:nameLabel];
//        nameLabel.backgroundColor = [UIColor redColor];
        
        //审核状态
        UILabel *stateLabel = [self createLabelWithFrame:CGRectMake(cX + 100 + 10, cY, cW - 100.0 - 10, 20.0) text:_activity.stateDescription font:[UIFont systemFontOfSize:14.0] textColor:[UIColor grayColor] multiLine:NO];
        stateLabel.font = [UIFont systemFontOfSize:13.0];
        stateLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:stateLabel];
//        stateLabel.backgroundColor = [UIColor yellowColor];
        
        cY = nameLabel.frame.origin.y + nameLabel.frame.size.height;
    }
    
    //正文
//    NSLog(@"view: info-b--%f", cY);
    if (showInfo) {
        UIView *contentView = [self createActivityRichViewWithFrame:CGRectMake(cX, cY + 5.0, cW, 0.0) title:_activity.title finance:_activity.finance content:_activity.content thumbnailMode:thumbnailMode];
        if (contentView != nil) {
            contentView.backgroundColor = [UIColor clearColor];
            [self addSubview:contentView];
            cY = contentView.frame.origin.y + contentView.frame.size.height;
        }
    }
    
    //附件视图
//    NSLog(@"view: appendix-b--%f", cY);
    if (showAppendix && _activity.isHaveAppendix) {
        UIView *appendixView = [self createAppendixViewWithFrame:CGRectMake(cX, cY + 5.0, cW, 0.0)
                                                        appendix:_activity.appendixDictionary
                                                   thumbnailMode:thumbnailMode];
        [self addSubview:appendixView];
        cY = appendixView.frame.origin.y + appendixView.frame.size.height;
    }
    
    //位置
//    NSLog(@"view: address-b--%f", cY);
    if (showAddress) {
        UIView *addressView = nil;
        if (addressStyle == XDAddressStyleText) {
            NSString *address = _activity.address.length ? _activity.address : @"定位失败";
            UIFont *font = [UIFont systemFontOfSize:11.0];
            CGSize size = [address sizeWithFont:font constrainedToSize:CGSizeMake(cW, 40.0)];
            
            addressView = [[UIView alloc] initWithFrame:CGRectMake(cX, cY + 5.0, cW, size.height)];
            addressView.backgroundColor = [UIColor clearColor];
            UIImageView *addressIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, addressView.frame.size.height)];
            addressIcon.contentMode = UIViewContentModeCenter;
            addressIcon.image = [UIImage imageNamed:@"address.png"];
            [addressView addSubview:addressIcon];
            UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(addressIcon.frame.origin.x + addressIcon.frame.size.width, 0, addressView.frame.size.width - (addressIcon.frame.origin.x + addressIcon.frame.size.width), addressView.frame.size.height)];
            addressLabel.numberOfLines = 0;
            addressLabel.backgroundColor = [UIColor clearColor];
            addressLabel.font = font;
            addressLabel.textColor = [UIColor grayColor];
            addressLabel.text = address;
            [addressView addSubview:addressLabel];
            [self addSubview:addressView];
        }
        else if (addressStyle == XDAddressStyleMap)
        {
            
        }
        
        cY = addressView.frame.origin.y + addressView.frame.size.height;
    }
    
    //审核按钮
    if (showAuditButton) {
        if (showAuditCount) {
            //????
        }
        else {
            //????
        }
        
        self.auditButton.frame = CGRectMake(cX + cW - 60.0, cY + 8.0, 60.0, 24.0);
        [self addSubview:self.auditButton];
    }
    else{
        _auditButton = nil;
    }
    
    //评论按钮
    if (showCommentButton)
    {
        if (showCommentCount) {
            //????
        }
        else {
            //????
        }
        
        CGFloat originX = cX + cW - 60.0;
        if (_auditButton != nil) {
            originX = _auditButton.frame.origin.x - 10;
        }
        
        UIButton *commentButton = [[UIButton alloc] initWithFrame:CGRectMake(originX - 60.0, cY + 8.0, 60.0, 24.0)];
        [commentButton setTitle:@"评论" forState:UIControlStateNormal];
        [commentButton addTarget:self action:@selector(clickCommentButton:) forControlEvents:UIControlEventTouchUpInside];
        [commentButton setImage:[UIImage imageNamed:@"audit_icon_comment.png"] forState:UIControlStateNormal];
        commentButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, 10.0, 0.0, 30.0);
        commentButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        commentButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [commentButton setTitleColor:[UIColor colorWithRed:113 / 255.0 green:132 / 255.0 blue:173 / 255.0 alpha:1.0] forState:UIControlStateNormal];
        [self addSubview:commentButton];
    }
    
    //时间、来源
//    NSLog(@"view: date-b--%f", cY);
    if (showTimeSource) {
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(cX, cY + 8.0, 100.0, 24.0)];
        dateLabel.font = [UIFont systemFontOfSize:14.0];
        dateLabel.textColor = [UIColor blackColor];
        dateLabel.backgroundColor = [UIColor clearColor];
        dateLabel.text = _activity.createDate.timeIntervalDescription;
        [self addSubview:dateLabel];
        
        cY = dateLabel.frame.origin.y + dateLabel.frame.size.height + 5;
    }
    
    //列表向上的箭头
//    UIImageView *arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(cX + 10.0, cY, 20, 10)];
//    arrowView.image = [UIImage imageNamed:@"audit_arrowUp.png"];
    
    //审核列表
//    NSLog(@"view: audit-b--%f", cY);
    UIView *auditView = nil;
    if (showAudits && _activity.isHaveAudit) {
        //添加向上箭头
//        [self addSubview:arrowView];
//        cY = arrowView.frame.origin.y + arrowView.frame.size.height;
        
        auditView = [self createAuditRichViewWithFrame:CGRectMake(cX, cY, cW, 0.0) auditList:_activity.auditList thumbnailMode:thumbnailMode];
        auditView.backgroundColor = [UIColor whiteColor];
        [self addSubview:auditView];
        
        cY = auditView.frame.origin.y + auditView.frame.size.height;
    }
    
    //评论列表
//    NSLog(@"view: comment-b--%f", cY);
    if (showComments && _activity.isHaveComment) {
        if (auditView == nil) {
            //添加向上箭头
//            [self addSubview:arrowView];
//            cY = arrowView.frame.origin.y + arrowView.frame.size.height;
        }
        else{
            cY += 5;
        }
        
        UIView *commentView = [self createCommentRichViewWithFrame:CGRectMake(cX, cY, cW, 0.0) commentList:_activity.commentList thumbnailMode:thumbnailMode];
        commentView.backgroundColor = [UIColor colorWithRed:232 / 255.0 green:231 / 255.0 blue:230 / 255.0 alpha:1.0];
        [self addSubview:commentView];
        
        cY = commentView.frame.origin.y + commentView.frame.size.height;
    }
    
    //修正高度
    cY += 10.0;
//    NSLog(@"view: end--%f", cY);
    
    CGRect frame = self.frame;
    frame.size.height = cY;
    self.frame = frame;
}

/*动态布局 7*/
- (void)setupActivityViewAbove7WithShowHead:(BOOL)showHead
                             showName:(BOOL)showName
                             showInfo:(BOOL)showInfo
                          showAddress:(BOOL)showAddress
                       showTimeSource:(BOOL)showTimeSource
                      showAuditButton:(BOOL)showAuditButton
                       showAuditCount:(BOOL)showAuditCount
                           showAudits:(BOOL)showAudits
                    showCommentButton:(BOOL)showCommentButton
                     showCommentCount:(BOOL)showCommentCount
                         showComments:(BOOL)showComments
                        thumbnailMode:(BOOL)thumbnailMode
{
    //UIText Kit
    //????
}

#pragma mark - public

- (CGFloat)activityViewHeight;
{
    return [self activityViewHeightWithShowHead:YES showName:YES showInfo:YES showAppendix:YES showAddress:YES showTimeSource:YES showAudits:YES showComments:YES thumbnailMode:YES];
}

- (void)setupActivityView
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
//        [self setupActivityViewAbove7WithShowHead:YES showName:YES showInfo:YES showAddress:YES showTimeSource:YES showAuditButton:YES showAuditCount:NO showAudits:YES showCommentButton:YES showCommentCount:NO showComments:YES thumbnailMode:YES];
        
        [self setupActivityViewWithShowHead:YES showName:YES showInfo:YES showAppendix:YES showAddress:YES addressStyle:XDAddressStyleText showTimeSource:YES showAuditButton:YES showAuditCount:NO showAudits:YES showCommentButton:YES showCommentCount:NO showComments:YES thumbnailMode:YES];
    }
    else{
        [self setupActivityViewWithShowHead:YES showName:YES showInfo:YES showAppendix:YES showAddress:YES addressStyle:XDAddressStyleText showTimeSource:YES showAuditButton:YES showAuditCount:NO showAudits:YES showCommentButton:YES showCommentCount:NO showComments:YES thumbnailMode:YES];
    }
}

#pragma mark - Action

/*点击评论按钮*/
- (void)clickCommentButton:(id)sender
{
    if ([_delegate respondsToSelector:@selector(contactActivityViewCommentButtonClicked:)]) {
        [_delegate contactActivityViewCommentButtonClicked:self];
    }
}

/*点击处理按钮*/
- (void)clickAuditButton:(id)sender
{
    if ([_delegate respondsToSelector:@selector(contactActivityViewAuditButtonClicked:)]) {
        [_delegate contactActivityViewAuditButtonClicked:self];
    }
}

@end
