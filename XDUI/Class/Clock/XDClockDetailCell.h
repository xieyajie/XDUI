//
//  XDClockDetailCell.h
//  XDUI
//
//  Created by xieyajie on 13-10-14.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XDPublicView;
@interface XDClockDetailCell : UITableViewCell
{
    XDPublicView *_publicDetailView;
    XDPublicView *_publicImageView;
    UIButton *_errorButton;
    
    UITableView *_rootTableView;
}

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *detailText;
@property (strong, nonatomic) UIColor *textColor;

- (void)showButtonToTableView:(UITableView *)tableView title:(NSString *)title;
- (void)setupWithImageSource:(NSArray *)array canEdit:(BOOL)edit;

@end
