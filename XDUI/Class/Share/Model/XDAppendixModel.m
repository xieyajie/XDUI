//
//  XDAppendixModel.m
//  XDUI
//
//  Created by xieyajie on 13-10-25.
//  Copyright (c) 2013年 XD. All rights reserved.
//

#import "XDAppendixModel.h"

#import "NSString+Category.h"

@implementation XDAppendixModel

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
	if (self) {
        self.ID = [NSString stringWithFormat:@"%d", [[dictionary objectForKey:KMODEL_ID] integerValue]];
        self.name = [dictionary objectForKey:KMODEL_NAME];
        self.duration = ceil([[dictionary objectForKey:KAPPENDIX_DURATION] floatValue] / 1000.0); //数据源单位毫秒
        self.size = [[dictionary objectForKey:KAPPENDIX_SIZE] floatValue];
        self.sourcePath = [dictionary objectForKey:KAPPENDIX_SOURCEPATH];
        
        self.mediumImagePath = [dictionary objectForKey:@"mediumImagePath"];
        self.smallImagePath = [dictionary objectForKey:@"smallImagePath"];
        self.relatedObject = [dictionary valueForKey:KAPPENDIX_RELATED];

//        if (!self.sourcePath.isDocumentFilePath) {
//            self.sourcePath = self.sourcePath.formattedImagePath;
//        }
//        
//        if (!self.mediumImagePath.isDocumentFilePath) {
//            self.mediumImagePath = self.mediumImagePath.formattedImagePath;
//            if (!self.mediumImagePath.isValidImageURL) {
//                self.mediumImagePath = nil;
//            }
//        }
//        
//        if (!self.smallImagePath.isDocumentFilePath) {
//            self.smallImagePath = self.smallImagePath.formattedImagePath;
//            if (!self.smallImagePath.isValidImageURL) {
//                self.smallImagePath = nil;
//            }
//        }
        
        if (!self.sourcePath.length) {
            if (self.mediumImagePath.length) {
                self.sourcePath = [self.mediumImagePath copy];                          //无源路径时，使用中图路径
            }
            else if (self.smallImagePath.length) {
                self.sourcePath = [self.smallImagePath copy];                           //无源路径和中图路径时，使用小图路径
            }
        }
        
        if (!self.sourcePath.isDocumentFilePath) {
//            if (self.mediumImagePath.length) {
//                if (!self.smallImagePath.length) {
//                    self.smallImagePath = self.mediumImagePath.imagePathFormatThumbnailSize;    //无小图时，从中图路径转换
//                }
//                self.mediumImagePath = self.mediumImagePath.imagePathFormatNormalSize;       //通常是大图路径，强制转换为中图
//            } else {
//                if (self.smallImagePath.length) {
//                    self.mediumImagePath = [self.smallImagePath copy];                           //无中图时，使用小图路径
//                }
//            }
            
            //此处源路径、小图路径和中图路径的三种情况: 1-三者都为空 2-三者都有值 3-源路径有值，小图路径和中图路径都为空
            if (self.sourcePath.isValidImageURL) {
                self.type = ([self.sourcePath hasSuffix:@"gif"] || [self.sourcePath hasSuffix:@"GIF"]) ? XDMediaTypeGIF : XDMediaTypeImage;
            }
            else if (self.sourcePath.isValidVideoURL) {
                self.type = XDMediaTypeVideo;
            }
            else if (self.sourcePath.isValidAudioURL) {
                self.type = XDMediaTypeAudio;
            }
            
//            if (self.type == XDMediaTypeImage) {
//                if (!self.mediumImagePath.length) {
//                    self.mediumImagePath = self.sourcePath.imagePathFormatNormalSize;            //无中图时，从大图路径转换
//                }
//                
//                if (!self.smallImagePath.length) {
//                    self.smallImagePath = self.sourcePath.imagePathFormatThumbnailSize;         //无小图时，从大图路径转换
//                }
//            }
        }
	}
    return self;
}

#pragma mark - Description

/*时长描述*/
- (NSString *)durationDescription
{
    NSUInteger d = _duration;
    if (d < 60) {
        return [NSString stringWithFormat:@"%ds", d];
    } else {
        return [NSString stringWithFormat:@"%d:%02d", d / 60, d % 60];
    }
}

/*大小描述*/
- (NSString *)sizeDescription
{
    CGFloat s = _size / 1024.0;
    if (s < 1024.0) {
        return [NSString stringWithFormat:@"%.1fKB", s];
    } else {
        return [NSString stringWithFormat:@"%.1fMB", s / 1024.0];
    }
}

@end
